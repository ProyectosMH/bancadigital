package com.empresa.bancamovil.Products.Models

import java.io.Serializable

class Account() : Serializable {

    var name: String = ""
    var balance: String = ""
    var availableBalance: String = ""
    var accNumber: String = ""

    constructor(name: String, balance: String, availableBalance: String, accNumber: String) : this() {
        this.name = name
        this.balance = balance
        this.availableBalance = availableBalance
        this.accNumber = accNumber
    }
}