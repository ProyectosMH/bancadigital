package com.empresa.bancamovil.Products.Activities

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.util.TypedValue
import android.view.*
import android.widget.*
import androidx.core.content.ContextCompat
import com.empresa.bancamovil.Products.Interfaces.IProductoView
import com.empresa.bancamovil.Products.fragments.AccountsFragment
import com.empresa.bancamovil.Products.fragments.LoansFragment
import com.empresa.bancamovil.R
import com.empresa.bancamovil.Utils.BaseActivity
import android.widget.TextView
import android.view.Gravity
import com.empresa.bancamovil.Account.Movements.Activities.MovementsAccountActivity
import com.empresa.bancamovil.Account.QRTransfers.Activities.QRTransfersActivity
import com.empresa.bancamovil.Account.ServicesPay.Activities.ServicesPayActivity
import com.empresa.bancamovil.Account.TransfersToOwnAcc.Activities.TransfersToOwnAccActivity
import com.empresa.bancamovil.Account.TransfersToThird.Activities.TransfersToThirdActivity
import com.empresa.bancamovil.Account.WithdrawalWithoutCard.Activities.WithdrawalWithoutCardActivity
import com.empresa.bancamovil.Products.Models.Account
import com.empresa.bancamovil.Utils.Properties
import es.dmoral.toasty.Toasty

class ProductsActivity : BaseActivity(), IProductoView {

    var ivBack: ImageView? = null
    var llAccounts: LinearLayout? = null
    var llLoans: LinearLayout? = null
    var rlView: RelativeLayout? = null
    var ivLogout: ImageView? = null
    var ivAccounts: ImageView? = null
    var ivLoans: ImageView? = null
    var tvAccounts: TextView? = null
    var tvLoans: TextView? = null
    lateinit var dialogMenu: Dialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_products)

        initialize()

        llAccounts!!.setOnClickListener {
            goAccountsFragment()
        }

        llLoans!!.setOnClickListener {
            goLoansFragment()
        }

        ivLogout!!.setOnClickListener {
            logout()
        }
    }

    //region initialize
    private fun initialize() {
        menu()

        ivBack = findViewById<ImageView>(R.id.ivBack)
        ivBack!!.visibility = View.GONE
        llAccounts = findViewById<LinearLayout>(R.id.llAccounts)
        llLoans = findViewById<LinearLayout>(R.id.llLoans)
        rlView = findViewById<RelativeLayout>(R.id.rlView)
        ivLogout = findViewById<ImageView>(R.id.ivLogout)
        ivAccounts = findViewById<ImageView>(R.id.ivAccounts)
        ivLoans = findViewById<ImageView>(R.id.ivLoans)
        tvAccounts = findViewById<TextView>(R.id.tvAccounts)
        tvLoans = findViewById<TextView>(R.id.tvLoans)
        ProgresbarStar()
        goAccountsFragment()
        createMenuDialog()
    }
    //endregion initialize

    //region view
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        return if (keyCode == KeyEvent.KEYCODE_BACK) {
            return false
        } else super.onKeyDown(keyCode, event)
    }

    private fun goAccountsFragment(){
        ivAccounts!!.setColorFilter(ContextCompat.getColor(applicationContext, R.color.colorPrimaryDark))
        ivLoans!!.setColorFilter(ContextCompat.getColor(applicationContext, R.color.colorSecondaryText))
        val accInDp = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 25F, resources.displayMetrics ).toInt()
        ivAccounts!!.layoutParams.height = accInDp
        ivAccounts!!.layoutParams.width = accInDp
        ivAccounts!!.requestLayout()
        val loanInDp = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20F, resources.displayMetrics ).toInt()
        ivLoans!!.layoutParams.height = loanInDp
        ivLoans!!.layoutParams.width = loanInDp
        ivLoans!!.requestLayout()
        tvAccounts!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorPrimaryDark))
        tvLoans!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorSecondaryText))

        val textFragment = AccountsFragment()
        val manager = supportFragmentManager
        val transaction = manager.beginTransaction()
        transaction.replace(R.id.fProducts, textFragment)
        transaction.addToBackStack(null)
        transaction.commit()
        ProgresbarFinish()
    }

    private fun goLoansFragment(){
        ivAccounts!!.setColorFilter(ContextCompat.getColor(applicationContext, R.color.colorSecondaryText))
        ivLoans!!.setColorFilter(ContextCompat.getColor(applicationContext, R.color.colorPrimaryDark))
        val accInDp = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20F, resources.displayMetrics ).toInt()
        ivAccounts!!.layoutParams.height = accInDp
        ivAccounts!!.layoutParams.width = accInDp
        ivAccounts!!.requestLayout()
        val loanInDp = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 25F, resources.displayMetrics ).toInt()
        ivLoans!!.layoutParams.height = loanInDp
        ivLoans!!.layoutParams.width = loanInDp
        ivLoans!!.requestLayout()
        tvAccounts!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorSecondaryText))
        tvLoans!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorPrimaryDark))

        val textFragment = LoansFragment()
        val manager = supportFragmentManager
        val transaction = manager.beginTransaction()
        transaction.replace(R.id.fProducts, textFragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    override fun OnSelectAccount(account: Account) {
        setValueSession(Properties.ACCOUNT_NAME, account.name, applicationContext, Properties.SESION_PREFERENCES)
        setValueSession(Properties.ACCOUNT_NUMBER, account.accNumber, applicationContext, Properties.SESION_PREFERENCES)
        setValueSession(Properties.ACCOUNT_AVAILABLE, account.availableBalance, applicationContext, Properties.SESION_PREFERENCES)
        showMenuDialog()
    }

    override fun OnProcesError(error: String) {
        Toasty.error(applicationContext, error, Toast.LENGTH_SHORT, true).show()
        ProgresbarFinish()
    }

    private fun createMenuDialog() {
        dialogMenu = Dialog(this, R.style.DialogThemeMenu)
        dialogMenu.setContentView(R.layout.menu_dialog)
        dialogMenu.window!!.setGravity(Gravity.RIGHT)
        dialogMenu.window.attributes.windowAnimations = R.style.DialogThemeMenu

        var llTransfersOwnAcc: LinearLayout = dialogMenu.findViewById<LinearLayout>(R.id.llTransfersOwnAcc)
        var llTransfersThird: LinearLayout = dialogMenu.findViewById<LinearLayout>(R.id.llTransfersThird)
        var llTransfersQR: LinearLayout = dialogMenu.findViewById<LinearLayout>(R.id.llTransfersQR)
        var llWithoutCard: LinearLayout = dialogMenu.findViewById<LinearLayout>(R.id.llWithoutCard)
        var llMovementsAcc: LinearLayout = dialogMenu.findViewById<LinearLayout>(R.id.llMovementsAcc)
        var llServicesPay: LinearLayout = dialogMenu.findViewById<LinearLayout>(R.id.llServicesPay)
        var llClose: LinearLayout = dialogMenu.findViewById<LinearLayout>(R.id.llClose)
        var tvAccNumber: TextView = dialogMenu.findViewById<TextView>(R.id.tvAccNumber)
        var tvAccAvailable: TextView = dialogMenu.findViewById<TextView>(R.id.tvAccAvailable)

        val accSession = getValueSession(Properties.ACCOUNT_NUMBER, applicationContext, Properties.SESION_PREFERENCES)
        val accAvaliable = getValueSession(Properties.ACCOUNT_AVAILABLE, applicationContext, Properties.SESION_PREFERENCES)
        tvAccNumber.text = accSession
        tvAccAvailable.text = "$" + formatNumber(accAvaliable)

        llTransfersOwnAcc.setOnClickListener {
            dialogMenu.dismiss()
            val intent = Intent(this, TransfersToOwnAccActivity::class.java)
            startActivity(intent)
        }

        llTransfersThird.setOnClickListener {
            dialogMenu.dismiss()
            val intent = Intent(this, TransfersToThirdActivity::class.java)
            startActivity(intent)
        }

        llTransfersQR.setOnClickListener {
            dialogMenu.dismiss()
            val intent = Intent(this, QRTransfersActivity::class.java)
            startActivity(intent)
        }

        llWithoutCard.setOnClickListener {
            dialogMenu.dismiss()
            val intent = Intent(this, WithdrawalWithoutCardActivity::class.java)
            startActivity(intent)
        }

        llMovementsAcc.setOnClickListener {
            dialogMenu.dismiss()
            val intent = Intent(this, MovementsAccountActivity::class.java)
            startActivity(intent)
        }

        llServicesPay.setOnClickListener {
            dialogMenu.dismiss()
            val intent = Intent(this, ServicesPayActivity::class.java)
            startActivity(intent)
        }

        llClose.setOnClickListener {
            dialogMenu.dismiss()
        }

        ProgresbarFinish()
    }

    fun showMenuDialog(){
        dialogMenu.show()
    }
    //endregion view
}
