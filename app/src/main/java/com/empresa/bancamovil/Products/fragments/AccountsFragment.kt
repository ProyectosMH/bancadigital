package com.empresa.bancamovil.Products.fragments

import android.app.Activity
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.empresa.bancamovil.Products.Models.Account

import com.empresa.bancamovil.R
import androidx.recyclerview.widget.LinearLayoutManager
import com.empresa.bancamovil.Products.Adapters.AccountAdapter
import com.empresa.bancamovil.Products.Interfaces.IProductoView


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [AccountsFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [AccountsFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class AccountsFragment() : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null
    lateinit var iProductoView: IProductoView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_accounts, container, false)
        val rvAccounts = view.findViewById(R.id.rvAccounts) as RecyclerView

        val accountList = ArrayList<Account>()

        //adding some dummy data to the list
        accountList.add(Account("Cuenta de ahorro", "$10.00", "$100", "1000-XXXX-XXXX-8745"))
        accountList.add(Account("Cuenta digital", "$11.00", "$98", "1000-XXXX-XXXX-4196"))
        accountList.add(Account("Cuenta corriente", "$12.00", "$95", "1000-XXXX-XXXX-7820"))

        val adapter = AccountAdapter(accountList, iProductoView)
        rvAccounts!!.layoutManager = LinearLayoutManager(context)
        rvAccounts!!.adapter = adapter

        return view
    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        try {
            iProductoView = getActivity() as IProductoView
        } catch (e: ClassCastException) {
            throw ClassCastException("$activity must implement onButtonPressed")
        }

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment AccountsFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            AccountsFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}
