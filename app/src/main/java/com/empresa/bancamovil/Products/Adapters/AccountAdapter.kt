package com.empresa.bancamovil.Products.Adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.empresa.bancamovil.Products.Models.Account
import com.empresa.bancamovil.R
import android.widget.LinearLayout
import android.util.Log
import com.empresa.bancamovil.Products.Interfaces.IProductoView

class AccountAdapter(
    private val accountsList: List<Account>,
    private val iProductoView: IProductoView
) : RecyclerView.Adapter<AccountAdapter.MyViewHolder>() {

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var numberProduct: TextView
        var name: TextView
        var balance: TextView
        var totalBalance : TextView
        var llAccount: LinearLayout

        init {
            numberProduct = view.findViewById(R.id.numberProduct) as TextView
            name = view.findViewById(R.id.name) as TextView
            balance = view.findViewById(R.id.balance) as TextView
            totalBalance = view.findViewById(R.id.totalBalance) as TextView
            llAccount = view.findViewById(R.id.llAccount) as LinearLayout
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.products_list, parent, false)

        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val account = accountsList.get(position)
        holder.numberProduct.text = account.accNumber
        holder.name.text = account.name
        holder.balance.text = account.balance
        holder.totalBalance.text = account.availableBalance

        holder.llAccount.setOnClickListener(View.OnClickListener {
            Log.d("Llego", "al click")
            iProductoView.OnSelectAccount(account)
        })
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return accountsList.size
    }
}