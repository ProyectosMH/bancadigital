package com.empresa.bancamovil.Products.Adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.empresa.bancamovil.R
import android.widget.LinearLayout
import android.util.Log
import com.empresa.bancamovil.Products.Models.Loan

class LoansAdapter(private val loansList: List<Loan>) : RecyclerView.Adapter<LoansAdapter.MyViewHolder>() {

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var numberProduct: TextView
        var name: TextView
        var balance: TextView
        var totalBalance : TextView
        var llAccount: LinearLayout

        init {
            numberProduct = view.findViewById(R.id.numberProduct) as TextView
            name = view.findViewById(R.id.name) as TextView
            balance = view.findViewById(R.id.balance) as TextView
            totalBalance = view.findViewById(R.id.totalBalance) as TextView
            llAccount = view.findViewById(R.id.llAccount) as LinearLayout
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.products_list, parent, false)

        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val loan = loansList.get(position)
        holder.numberProduct.text = loan.loanNumber
        holder.name.text = loan.name
        holder.balance.text = loan.balance
        holder.totalBalance.text = loan.monthlyFee

        holder.llAccount.setOnClickListener(View.OnClickListener {
            Log.d("Llego", "al click")
        })
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return loansList.size
    }
}