package com.empresa.bancamovil.Products.Interfaces

import com.empresa.bancamovil.Products.Models.Account

interface IProductoView {
    fun OnSelectAccount(account: Account)
    fun OnProcesError(error: String)
}