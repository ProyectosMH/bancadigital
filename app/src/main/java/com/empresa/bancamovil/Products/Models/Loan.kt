package com.empresa.bancamovil.Products.Models

import java.io.Serializable

class Loan() : Serializable {

    var name: String = ""
    var balance: String = ""
    var monthlyFee: String = ""
    var loanNumber: String = ""

    constructor(name: String, balance: String, monthlyFee: String, loanNumber: String) : this() {
        this.name = name
        this.balance = balance
        this.monthlyFee = monthlyFee
        this.loanNumber = loanNumber
    }
}