package com.empresa.bancamovil.Utils

class Properties {
    companion object {
        @JvmField val USER_BIOMETRIC = "USUARIO_F"
        @JvmField val PASS_BIOMETRIC = "PASS_F"
        @JvmField val BIOMETRIC_PREFERENCES = "BiometricAuth_Prefe"

        @JvmField val SESION_PREFERENCES = "Session_Preferences_A"
        @JvmField val USER_NAME = "User_Name_S"
        @JvmField val USER_PASS = "User_Pass_S"
        @JvmField val ACCOUNT_NAME = "Acc_Name_S"
        @JvmField val ACCOUNT_NUMBER = "Acc_Number_S"
        @JvmField val ACCOUNT_AVAILABLE = "Acc_Available_S"
    }
}