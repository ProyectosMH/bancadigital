package com.empresa.bancamovil.Utils;

import android.app.Application;
import android.content.Context;

import java.util.Timer;
import java.util.TimerTask;

public class BancaMovilApp extends Application {
    private static Application instance;
    private SessionInterface listener;
    Timer timer;
    public static final long timeLogout = 1000 * 60 * 5;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    public static Context getContext() {
        return instance.getApplicationContext();
    }

    public void starUserSession() {
        cancelTimer();

        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                listener.onSessionLogout();
            }
        }, timeLogout);
    }

    private void cancelTimer() {
        if (timer != null) timer.cancel();
    }

    public void registerUserSessionListener(SessionInterface listener) {
        this.listener = listener;
    }

    public void onUserInteraction() {
        starUserSession();
    }
}