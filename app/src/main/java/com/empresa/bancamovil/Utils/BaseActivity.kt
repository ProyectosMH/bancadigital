package com.empresa.bancamovil.Utils

import android.app.AlertDialog
import android.app.Dialog
import android.app.KeyguardManager
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.PorterDuff
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.Window
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.navigation.NavigationView
import androidx.annotation.Nullable
import androidx.drawerlayout.widget.DrawerLayout
import com.empresa.bancamovil.R
import java.util.*
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.fragment.app.FragmentActivity
import es.dmoral.toasty.Toasty
import java.util.concurrent.Executors
import androidx.biometric.BiometricPrompt
import com.empresa.bancamovil.Login.Activities.LoginActivity
import com.empresa.bancamovil.Login.Interfaces.IBiometricAuthView
import com.empresa.bancamovil.Products.Activities.ProductsActivity
import java.text.DecimalFormat
import java.util.Calendar.*

open class BaseActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener, SessionInterface {

    private var pausedMillis: Long = getInstance().getTimeInMillis()
    public final var timeLogout: Long = 1000 * 60 * 5

    lateinit var drawerLayout: DrawerLayout
    lateinit var toolbar: Toolbar
    lateinit var toggle: ActionBarDrawerToggle
    lateinit var navigationView: NavigationView
    lateinit var menu: Menu
    lateinit var dialogLoading: Dialog

    private var fingerExist = false
    var hasEnrolledFinger = false
    private var iBiometricAuthView: IBiometricAuthView? = null

    override fun onCreate(@Nullable savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        (application as BancaMovilApp).registerUserSessionListener(this)
        (application as BancaMovilApp).starUserSession()

        if(this is LoginActivity){
            iBiometricAuthView = this as IBiometricAuthView
        }
    }

    override fun onUserInteraction() {
        super.onUserInteraction()

        if (this !is LoginActivity) {
            (application as BancaMovilApp).onUserInteraction()
        }
    }

    override fun onStop() {
        super.onStop()

        pausedMillis = getInstance().getTimeInMillis()
    }

    override fun onResume() {
        super.onResume()

        try {
            val actividad = this.javaClass.simpleName
            val currentMillis: Long?
            currentMillis = Calendar.getInstance().getTimeInMillis();
            //validatesesionTime()
        } catch (e: Exception) {

        }
    }

    fun validatesesionTime() {
        val actividad = this.javaClass.simpleName
        if (actividad != "MainActivity" && actividad != "LoginActivity") {
            val intent = Intent(this, LoginActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            startActivity(intent)
            finish()
        }
    }

    override fun onSessionLogout() {
        validatesesionTime()
    }

    fun goHomeProducts(){
        val intent = Intent(this, ProductsActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        this.startActivity(intent)
        this.finish()
    }

    fun logout() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Cerrar sesión")
        var icon: Drawable? = null
        icon = resources.getDrawable(R.drawable.ic_warning_24dp)
        icon.setColorFilter(resources.getColor(R.color.colorPrimaryDark), PorterDuff.Mode.SRC_IN)
        builder.setIcon(icon)
        builder.setMessage("¿Estas seguro que quieres salir?")
        builder.setCancelable(false)

        builder.setPositiveButton("Si"){dialog, which ->
            val intent = Intent(this, LoginActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            this.startActivity(intent)
            this.finish()
        }

        builder.setNegativeButton("No"){dialog,which ->
            //Toast.makeText(applicationContext,"You are not agree.",Toast.LENGTH_SHORT).show()
        }

        var alertDialog = builder.create()
        alertDialog.show()
    }

    fun menu() {
        navigationView = findViewById(R.id.nav_view) as NavigationView
        navigationView.setNavigationItemSelectedListener(this)
        //navigationView.itemIconTintList = null
        var toolbar = findViewById(R.id.toolbar) as Toolbar

        setSupportActionBar(toolbar)
        supportActionBar!!.setTitle("")
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        val header = navigationView.getHeaderView(0)
        val tvUser = header.findViewById(R.id.tvUser) as TextView
        val userSession = getValueSession(Properties.USER_NAME, applicationContext, Properties.SESION_PREFERENCES)
        tvUser.text = userSession

        if (verifyFingerDevice()) {
            menu = navigationView.menu
            val userBiometric = getValueSession(Properties.USER_BIOMETRIC, applicationContext, Properties.BIOMETRIC_PREFERENCES)

            if (!userBiometric.equals("default value")) {
                menu.findItem(R.id.iActivarHuella).isVisible = false
            } else {
                menu.findItem(R.id.iDesactivarHuella).isVisible = false
            }
        } else {
            menu = navigationView.menu
            menu.findItem(R.id.iActivarHuella).isVisible = false
            menu.findItem(R.id.iDesactivarHuella).isVisible = false
        }

        //menu = navigationView.menu

        toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(findViewById(R.id.toolbar) as Toolbar)
        drawerLayout = findViewById(R.id.drawer_layout) as DrawerLayout
        toggle = ActionBarDrawerToggle(
            this,
            drawerLayout,
            toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == R.id.iLogout) {
            logout()
        } else if (id == R.id.iActivarHuella) {
            showBiometricPrompt()
        } else if (id == R.id.iDesactivarHuella) {
            disabledBiometric()
        } else if(id == R.id.iChangePass){
            Toasty.success(applicationContext, "Cambiar contraseña", Toast.LENGTH_SHORT, true).show()
        } else {
            Toasty.error(applicationContext, "Menu sin funcionabilidad", Toast.LENGTH_SHORT, true).show()
        }

        drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout)
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    fun verifyFingerDevice(): Boolean {
        fingerExist = true
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (Build.VERSION.SDK_INT < 29) {
                val keyguardManager : KeyguardManager = applicationContext.getSystemService(KEYGUARD_SERVICE) as KeyguardManager
                val packageManager : PackageManager = applicationContext.packageManager
                if(!packageManager.hasSystemFeature(PackageManager.FEATURE_FINGERPRINT)) {
                    fingerExist = false
                }
                if (!keyguardManager.isKeyguardSecure) {
                    fingerExist = false
                }
            }
        }else{
            fingerExist = false
        }
        return fingerExist
    }

    fun showBiometricPrompt() {
        val executor = Executors.newSingleThreadExecutor()
        val activity: FragmentActivity = this
        val biometricPrompt = BiometricPrompt(activity, executor, object : BiometricPrompt.AuthenticationCallback() {

            override fun onAuthenticationError(errorCode: Int, errString: CharSequence) {
                super.onAuthenticationError(errorCode, errString)
                if (errorCode == BiometricPrompt.ERROR_NEGATIVE_BUTTON) {
                    // user clicked negative button
                } else {

                }
            }

            override fun onAuthenticationSucceeded(result: BiometricPrompt.AuthenticationResult) {
                super.onAuthenticationSucceeded(result)
                activity.runOnUiThread(Runnable {
                    iBiometricAuthView!!.OnBiometricAuthSuccess()
                })
            }

        })

        val promptInfo = BiometricPrompt.PromptInfo.Builder()
            .setTitle("Autenticación Biometrica")
            .setSubtitle("Usa tu credencial biometrica")
            .setNegativeButtonText("Cancelar")
            .build()

        biometricPrompt.authenticate(promptInfo)
    }

    fun biometricEnabled(){
        val biometricUser = getValueSession(Properties.USER_BIOMETRIC, applicationContext, Properties.BIOMETRIC_PREFERENCES)
        if(!biometricUser.equals("default value")){
            if(verifyFingerDevice()){
                showBiometricPrompt()
                hasEnrolledFinger = true
            }
        }
    }

    fun disabledBiometric(){
        val sharedpreferences = getSharedPreferences(Properties.BIOMETRIC_PREFERENCES, applicationContext)
        var editor = sharedpreferences.edit()
        editor.clear()
        editor.commit()
        /*finish()
        startActivity(intent)*/
        menu.findItem(R.id.iActivarHuella).isVisible = true
        menu.findItem(R.id.iDesactivarHuella).isVisible = false
        Toasty.success(applicationContext, "Huella desactivada exitosamente", Toast.LENGTH_SHORT).show()
    }

    fun setUserLoginSession(user: String, pass: String, typeLogin: String){
        setValueSession(Properties.USER_NAME, user, applicationContext, Properties.SESION_PREFERENCES)
        setValueSession(Properties.USER_PASS, pass, applicationContext, Properties.SESION_PREFERENCES)
        if(typeLogin == "biometric"){
            setValueSession(Properties.USER_BIOMETRIC, user, applicationContext, Properties.BIOMETRIC_PREFERENCES)
            setValueSession(Properties.PASS_BIOMETRIC, pass, applicationContext, Properties.BIOMETRIC_PREFERENCES)
        }
    }

    fun setValueSession(key: String, value: String, context: Context, tipoPrefe: String) {
        val sharedPreferences = getSharedPreferences(tipoPrefe, context)
        var edit = sharedPreferences.edit()
        edit.putString(key, value)
        edit.commit()
    }

    fun getValueSession(key: String, context: Context, preferencesType: String): String {
        val sharedPreferences = getSharedPreferences(preferencesType, context)
        if (sharedPreferences != null) {
            return sharedPreferences.getString(key, "default value")
        }
        return "default value"
    }

    fun getSharedPreferences(preferencesType: String, context: Context): SharedPreferences {
        return context.getSharedPreferences(preferencesType, 0)
    }

    fun ProgresbarStar() {
        dialogLoading = Dialog(this)
        dialogLoading.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogLoading.setCancelable(false)
        dialogLoading.setContentView(R.layout.layout_progress_bar)
        dialogLoading.show()
        var window = dialogLoading.window
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
    }

    fun ProgresbarFinish() {
        if(dialogLoading != null){
            if(dialogLoading.isShowing){
                dialogLoading.dismiss()
            }
        }
    }

    fun formatNumber(value: String): String{
        var montoFormat: String = value
        try{
           if(montoFormat != null){
               if(!montoFormat.equals("")){
                   montoFormat = montoFormat.replace("$", "")
                   montoFormat = montoFormat.replace(",", "")
                   var formatter = DecimalFormat("###,###,##0.00")
                   montoFormat = formatter.format(montoFormat.toDoubleOrNull())
               }
           }
        }catch (e: Exception){
            return montoFormat
        }
        return montoFormat
    }
}