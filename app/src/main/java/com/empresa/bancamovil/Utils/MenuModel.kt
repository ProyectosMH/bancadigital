package com.empresa.bancamovil.Utils

import java.io.Serializable

class MenuModel() : Serializable {

    var menuName: String = ""
    var url: String = ""
    var hasChildren: Boolean = false
    var isGroup: Boolean = false

    constructor(menuName: String, url: String, hasChildren: Boolean, isGroup: Boolean) : this() {
        this.menuName = menuName
        this.url = url
        this.hasChildren = hasChildren
        this.isGroup = isGroup
    }
}