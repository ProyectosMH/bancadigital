package com.empresa.bancamovil.Account.WithdrawalWithoutCard.Presenters

import android.content.Context
import com.empresa.bancamovil.Account.WithdrawalWithoutCard.Interfaces.IWithoutCardPresenter
import com.empresa.bancamovil.Account.WithdrawalWithoutCard.Interfaces.IWithoutCardView
import com.empresa.bancamovil.Account.WithdrawalWithoutCard.Models.TransferWithoutCard

public class TransferWithoutCardPresenter(iWithoutCardView: IWithoutCardView, context: Context) : IWithoutCardPresenter {

    private var iWithoutCardView = iWithoutCardView
    private var context = context
    private var repository = TransferWithoutCard()

    override fun validateData(transfer: TransferWithoutCard) {
        if(transfer.accName == ""){
            iWithoutCardView!!.OnProcesError("Por favor selecciona una cuenta")
        }else if(transfer.amount == ""){
            iWithoutCardView!!.OnProcesError("Ingresa el monto de la transferencia")
        }else{
            iWithoutCardView.OnValidateData(transfer)
        }
    }

    override fun validateTransfer(transfer: TransferWithoutCard) {
        iWithoutCardView.OnValidateData(transfer)
    }

    override fun OnProcessError(error: String) {
        iWithoutCardView!!.OnProcesError(error)
    }
}