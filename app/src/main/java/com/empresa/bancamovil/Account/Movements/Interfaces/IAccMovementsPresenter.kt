package com.empresa.bancamovil.Account.Movements.Interfaces

import com.empresa.bancamovil.Account.Movements.Models.AccMovement

interface IAccMovementsPresenter {
    fun getAccMovements()

    fun OnGetAccMovementsSuccess(movementsList: List<AccMovement>)
    fun OnProcessError(error: String)
}