package com.empresa.bancamovil.Account.QRTransfers.Activities

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.widget.*
import com.dev.materialspinner.MaterialSpinner
import com.empresa.bancamovil.Account.QRTransfers.Interfaces.IQRTransfersView
import com.empresa.bancamovil.Account.QRTransfers.Models.QRTransfer
import com.empresa.bancamovil.Account.QRTransfers.Presenters.QRTransfersPresenter
import com.empresa.bancamovil.R
import com.empresa.bancamovil.Utils.BaseActivity
import com.empresa.bancamovil.Utils.Properties
import com.google.android.material.textfield.TextInputEditText
import es.dmoral.toasty.Toasty
import net.glxn.qrgen.android.QRCode
import android.graphics.drawable.BitmapDrawable
import android.provider.MediaStore
import android.net.Uri
import android.os.Build
import androidx.core.app.ActivityCompat
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Log
import androidx.core.content.ContextCompat
import com.google.zxing.*
import com.google.zxing.common.HybridBinarizer
import com.google.zxing.integration.android.IntentIntegrator
import java.lang.Exception
import java.util.*
import kotlin.collections.ArrayList

class QRTransfersActivity : BaseActivity(), IQRTransfersView, AdapterView.OnItemSelectedListener {

    private var presenter: QRTransfersPresenter? = null
    var ivLogout: ImageView? = null
    var ivBack: ImageView? = null
    var tvAcc: TextView? = null
    var tvAccAvailable: TextView? = null
    private lateinit var msMetodo: MaterialSpinner
    var llSendQR: LinearLayout? = null
    var llReceiveQR: LinearLayout? = null
    var tivAmount: TextInputEditText? = null
    var tivDetail: TextInputEditText? = null
    var btnContinue: Button? = null
    var btnReadQR: Button? = null
    var btnLoadQR: Button? = null
    var accSession: String = ""
    var action = ""
    var permissions =
        arrayOf<String>(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
    val MULTIPLE_PERMISSIONS = 10

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_qrtransfers)

        initialize()
        presenter = QRTransfersPresenter(this, applicationContext)

        ivLogout!!.setOnClickListener {
            logout()
        }

        ivBack!!.setOnClickListener {
            finish()
        }

        btnContinue!!.setOnClickListener {
            var transfer = QRTransfer()
            transfer.accName = getValueSession(Properties.ACCOUNT_NAME, applicationContext, Properties.SESION_PREFERENCES)
            transfer.accNumber = accSession
            transfer.amount = tivAmount!!.text.toString()
            transfer.details = tivDetail!!.text.toString()
            presenter!!.validateData(transfer)
        }

        btnReadQR!!.setOnClickListener {
            action = "scan"
            scanQR()
        }

        btnLoadQR!!.setOnClickListener {
            action = "load"
            findQRFRomStorage()
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            //Check permissions for Android 6.0+
            checkPermissions()
        }
    }

    //region initialize
    private fun initialize() {
        ivLogout = findViewById<ImageView>(R.id.ivLogout)
        ivBack = findViewById<ImageView>(R.id.ivBack)
        tvAcc = findViewById<TextView>(R.id.tvAcc)
        tvAccAvailable = findViewById<TextView>(R.id.tvAccAvailable)
        msMetodo = findViewById<MaterialSpinner>(R.id.msMetodo)
        msMetodo.getSpinner().onItemSelectedListener = this
        llSendQR = findViewById<LinearLayout>(R.id.llSendQR)
        llReceiveQR = findViewById<LinearLayout>(R.id.llReceiveQR)
        tivAmount = findViewById<TextInputEditText>(R.id.tivAmount)
        tivDetail = findViewById<TextInputEditText>(R.id.tivDetail)
        btnContinue = findViewById<Button>(R.id.btnContinue)
        btnReadQR = findViewById<Button>(R.id.btnReadQR)
        btnLoadQR = findViewById<Button>(R.id.btnLoadQR)

        accSession = getValueSession(Properties.ACCOUNT_NUMBER, applicationContext, Properties.SESION_PREFERENCES)
        tvAcc!!.text = accSession
        tvAccAvailable!!.text = "$" + formatNumber(getValueSession(Properties.ACCOUNT_AVAILABLE, applicationContext, Properties.SESION_PREFERENCES))
        fillSpiner()
    }
    //endregion initialize

    //region view
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        return if (keyCode == KeyEvent.KEYCODE_BACK) {
            return false
        } else super.onKeyDown(keyCode, event)
    }

    fun fillSpiner(){
        var list_of_items = arrayOf("Selecciona un tipo de transferencia","Enviar transferencia QR", "Cobrar un código QR")
        val aa = ArrayAdapter(this, android.R.layout.simple_spinner_item, list_of_items)
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        msMetodo!!.setAdapter(aa)
    }

    override fun onItemSelected(arg0: AdapterView<*>, arg1: View, position: Int, id: Long) {
        if(position == 0)
        {
            llSendQR!!.visibility = View.GONE
            llReceiveQR!!.visibility = View.GONE
            //msMetodo.setError("Please select Country")
        }
        else
        {
            msMetodo.setErrorEnabled(false)
            msMetodo.setLabel("Metodo seleccionado")
            if(position == 1){
                llSendQR!!.visibility = View.VISIBLE
                llReceiveQR!!.visibility = View.GONE
            }else if(position == 2){
                llSendQR!!.visibility = View.GONE
                llReceiveQR!!.visibility = View.VISIBLE
            }
        }
    }

    override fun onNothingSelected(arg0: AdapterView<*>) {
    }

    override fun OnValidateData(transfer: QRTransfer) {
        var trama: String = transfer.accNumber + "~" + transfer.amount + "~" + transfer.details
        val bitmap = QRCode.from(trama).withSize(700, 700).bitmap()

        val dialog = Dialog(this)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.qr_dialog)

        var ivQR: ImageView = dialog.findViewById<ImageView>(R.id.ivQR)
        var btnSendQR: Button = dialog.findViewById<Button>(R.id.btnSendQR)
        var btnCancel: Button = dialog.findViewById<Button>(R.id.btnCancel)

        ivQR.setImageBitmap(bitmap)

        btnSendQR.setOnClickListener {
            var mDrawable = ivQR.drawable
            var mBitmap = (mDrawable as BitmapDrawable).bitmap

            val path = MediaStore.Images.Media.insertImage(contentResolver, mBitmap, "Image Description", null)
            val uri = Uri.parse(path)

            val intent = Intent(Intent.ACTION_SEND)
            intent.type = "image/jpeg"
            intent.putExtra(Intent.EXTRA_STREAM, uri)
            startActivity(Intent.createChooser(intent, "Share Image"))
            tivAmount!!.setText("")
            tivDetail!!.setText("")
        }

        btnCancel.setOnClickListener {
            tivAmount!!.setText("")
            tivDetail!!.setText("")
            dialog.dismiss()
        }

        dialog.show()
    }

    override fun OnProcesError(error: String) {
        Toasty.error(applicationContext, error, Toast.LENGTH_SHORT, true).show()
    }

    private fun checkPermissions(): Boolean {
        var result: Int
        val listPermissionsNeeded = ArrayList<String>()
        for (p in permissions) {
            result = ContextCompat.checkSelfPermission(this, p)
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p)
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(
                this,
                listPermissionsNeeded.toTypedArray(),
                MULTIPLE_PERMISSIONS
            )
            return false
        }
        return true
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissionsList: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            MULTIPLE_PERMISSIONS -> {
                if (grantResults.size > 0) {
                    var permissionsDenied = ""
                    for (per in permissionsList) {
                        if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                            permissionsDenied += "\n" + per
                        }
                    }
                }
                return
            }
        }
    }

    fun scanQR() {
        val intent = IntentIntegrator(this)
        intent.setBeepEnabled(false)
        intent.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES)

        intent.setPrompt("Escaneando código QR")
        intent.setCameraId(0)
        intent.initiateScan()
    }

    fun findQRFRomStorage() {
        val pickPhoto = Intent(Intent.ACTION_PICK)
        pickPhoto.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*")
        startActivityForResult(pickPhoto, 111)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (action.equals("scan")) {
            val result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
            if (result != null) {
                if (result.contents == null) {
                    OnProcesError("Cancelaste el escaneo")
                } else {
                    var trama = result.contents.toString()
                    var tramaArray = trama.split("~")
                    if(tramaArray.count() == 3){
                        var transfer = QRTransfer()
                        transfer.accName = getValueSession(Properties.ACCOUNT_NAME, applicationContext, Properties.SESION_PREFERENCES)
                        transfer.accNumber = tramaArray[0]
                        transfer.amount = tramaArray[1]
                        transfer.details = tramaArray[2]

                        val intent = Intent(this, ValidateQRTransfersActivity::class.java)
                        intent.putExtra("TRANSFER", transfer)
                        startActivity(intent)
                    }else{
                        OnProcesError("Código QR incorrecto")
                    }
                }
            } else {
                super.onActivityResult(requestCode, resultCode, data)
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)

            if (requestCode == 2 && resultCode == Activity.RESULT_OK || (requestCode == 111 && resultCode == Activity.RESULT_OK && null != data)) {
                var picturePath = ""
                try {
                    val selectedImage: Uri = data!!.data
                    val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)

                    val cursor = contentResolver.query(selectedImage!!, filePathColumn, null, null, null)
                    cursor!!.moveToFirst()

                    val columnIndex = cursor.getColumnIndex(filePathColumn[0])
                    picturePath = cursor.getString(columnIndex)
                    cursor.close()

                    var bitmap = BitmapFactory.decodeFile(picturePath)
                    decodeQRCode(bitmap)
                }catch (e: Exception){
                    Log.i("Error", "resultCode: ${e.message}")
                }
            } else {
                Log.i("SonaSys", "resultCode: $resultCode")
            }
        }

        action = ""
    }

    fun decodeQRCode(QRImage: Bitmap){
        try {
            var trama: String
            var imageResize = getResizedBitmap(QRImage, 550)

            var pixels = IntArray(imageResize.width * imageResize.height)
            imageResize.getPixels(pixels, 0, imageResize.width, 0, 0, imageResize.width, imageResize.height)
            var source: LuminanceSource = RGBLuminanceSource(imageResize.width, imageResize.height, pixels)
            val bitmap = BinaryBitmap(HybridBinarizer(source))
            var reader: Reader = MultiFormatReader()
            val decodeHints = Hashtable<DecodeHintType, Any>()
            decodeHints[DecodeHintType.TRY_HARDER] = java.lang.Boolean.TRUE
            decodeHints[DecodeHintType.PURE_BARCODE] = java.lang.Boolean.TRUE


            var result: Result = reader.decode(bitmap, decodeHints)
            trama = result.text.toString()
            var tramaArray = trama.split("~")
            if(tramaArray.count() == 3){
                var transfer = QRTransfer()
                transfer.accName = getValueSession(Properties.ACCOUNT_NAME, applicationContext, Properties.SESION_PREFERENCES)
                transfer.accNumber = tramaArray[0]
                transfer.amount = tramaArray[1]
                transfer.details = tramaArray[2]

                val intent = Intent(this, ValidateQRTransfersActivity::class.java)
                intent.putExtra("TRANSFER", transfer)
                startActivity(intent)
            }else{
                OnProcesError("Código QR incorrecto")
            }
        }catch (e: Exception){
            OnProcesError("Código QR incorrecto")
        }
    }

    fun getResizedBitmap(image: Bitmap, maxSize: Int): Bitmap {
        var width = image.width
        var height = image.height

        var bitmapRatio = width / height
        if (bitmapRatio > 1) {
            width = maxSize
            height = (width / bitmapRatio)
        } else {
            height = maxSize
            width = (height * bitmapRatio)
        }
        return Bitmap.createScaledBitmap(image, width, height, true)
    }
    //endregion view
}
