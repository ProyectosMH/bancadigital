package com.empresa.bancamovil.Account.QRTransfers.Activities

import android.content.Intent
import android.os.Bundle
import android.view.KeyEvent
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.empresa.bancamovil.Account.QRTransfers.Interfaces.IQRTransfersView
import com.empresa.bancamovil.Account.QRTransfers.Models.QRTransfer
import com.empresa.bancamovil.Account.QRTransfers.Presenters.QRTransfersPresenter
import com.empresa.bancamovil.R
import com.empresa.bancamovil.Utils.BaseActivity
import com.empresa.bancamovil.Utils.Properties
import es.dmoral.toasty.Toasty

class ValidateQRTransfersActivity : BaseActivity(), IQRTransfersView {

    private var presenter: QRTransfersPresenter? = null
    var ivLogout: ImageView? = null
    var ivBack: ImageView? = null
    var tvOriginAcc: TextView? = null
    var tvAccName: TextView? = null
    var tvDestiAcc: TextView? = null
    var tvAmount: TextView? = null
    var tvDetails: TextView? = null
    var btnContinue: Button? = null
    lateinit var transfer: QRTransfer
    var accSession: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_validate_qrtransfers)

        initialize()
        presenter = QRTransfersPresenter(this, applicationContext)

        ivLogout!!.setOnClickListener {
            logout()
        }

        ivBack!!.setOnClickListener {
            finish()
        }

        btnContinue!!.setOnClickListener {
            presenter!!.validateTransfer(transfer)
        }
    }

    //region initialize
    private fun initialize() {
        ivLogout = findViewById<ImageView>(R.id.ivLogout)
        ivBack = findViewById<ImageView>(R.id.ivBack)
        tvOriginAcc = findViewById<TextView>(R.id.tvOriginAcc)
        tvAccName = findViewById<TextView>(R.id.tvAccName)
        tvDestiAcc = findViewById<TextView>(R.id.tvDestiAcc)
        tvAmount = findViewById<TextView>(R.id.tvAmount)
        tvDetails = findViewById<TextView>(R.id.tvDetails)
        btnContinue = findViewById<Button>(R.id.btnContinue)

        transfer = intent.extras.getSerializable("TRANSFER") as QRTransfer
        fillData()
    }
    //endregion initialize

    //region view
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        return if (keyCode == KeyEvent.KEYCODE_BACK) {
            return false
        } else super.onKeyDown(keyCode, event)
    }

    fun fillData(){
        if(transfer != null){
            val accSession = getValueSession(Properties.ACCOUNT_NUMBER, applicationContext, Properties.SESION_PREFERENCES)
            tvOriginAcc!!.text = transfer.accNumber
            tvAccName!!.text = getValueSession(Properties.ACCOUNT_NAME, applicationContext, Properties.SESION_PREFERENCES)
            tvDestiAcc!!.text = accSession
            tvAmount!!.text = "$" + formatNumber(transfer!!.amount)
            tvDetails!!.text = transfer!!.details
        }
    }

    override fun OnValidateData(transfer: QRTransfer) {
        val intent = Intent(this, VoucherQRTransfersActivity::class.java)
        intent.putExtra("TRANSFER", transfer)
        startActivity(intent)
    }

    override fun OnProcesError(error: String) {
        Toasty.error(applicationContext, error, Toast.LENGTH_SHORT, true).show()
    }
    //endregion view
}
