package com.empresa.bancamovil.Account.ServicesPay.Interfaces

import com.empresa.bancamovil.Account.ServicesPay.Models.ServicePay

interface IServiPayView {
    fun OnValidateData(pay: ServicePay)
    fun OnProcesError(error: String)
}