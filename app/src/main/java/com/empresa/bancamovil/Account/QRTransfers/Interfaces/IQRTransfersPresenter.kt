package com.empresa.bancamovil.Account.QRTransfers.Interfaces

import com.empresa.bancamovil.Account.QRTransfers.Models.QRTransfer

interface IQRTransfersPresenter {
    fun validateData(transfer: QRTransfer)
    fun validateTransfer(transfer: QRTransfer)

    fun OnProcessError(error: String)
}