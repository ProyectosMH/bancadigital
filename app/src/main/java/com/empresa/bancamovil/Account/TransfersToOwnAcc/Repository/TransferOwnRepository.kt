package com.empresa.bancamovil.Account.TransfersToOwnAcc.Repository

import com.empresa.bancamovil.Account.TransfersToOwnAcc.Interfaces.ITransferOwnPresenter
import com.empresa.bancamovil.Products.Models.Account

class TransferOwnRepository {

    fun getAccounts(accSession: String, presenter: ITransferOwnPresenter) {
        try {
            var counter: Int = 0
            val accountsList = ArrayList<Account>()
            val accountsListCorrect = ArrayList<Account>()

            //adding some dummy data to the list
            accountsList.add(Account("Cuenta de Ahorro MiPago", "$10.00", "$100", "1000-XXXX-XXXX-8745"))
            accountsList.add(Account("Cuenta ahorro simplificada", "$11.00", "$98", "1000-XXXX-XXXX-4196"))
            accountsList.add(Account("Cuenta corriente", "$12.00", "$95", "1000-XXXX-XXXX-7820"))

            for (account in accountsList) {
                if(account.accNumber != accSession){
                    accountsListCorrect.add(account)
                }
            }

            presenter.OnGetAccountsSuccess(accountsListCorrect)
        } catch (e: Exception) {
            presenter.OnProcessError("Estamos en mantenimiento, intenta nuevamente")
        }
    }
}