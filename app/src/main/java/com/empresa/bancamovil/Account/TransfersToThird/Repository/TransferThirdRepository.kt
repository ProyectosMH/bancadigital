package com.empresa.bancamovil.Account.TransfersToThird.Repository

import com.empresa.bancamovil.Account.TransfersToThird.Interfaces.ITransferThirdPresenter
import com.empresa.bancamovil.Account.TransfersToThird.Models.Favorite

class TransferThirdRepository {

    fun getFavorites(presenter: ITransferThirdPresenter) {
        try {
            val favoritesList = ArrayList<Favorite>()

            //adding some dummy data to the list
            favoritesList.add(Favorite("Juan Perez", "1000-XXXX-XXXX-8520", "1"))
            favoritesList.add(Favorite("Marcos Palacios", "1000-XXXX-XXXX-9632", "2"))
            favoritesList.add(Favorite("Santiago Martinez", "1000-XXXX-XXXX-7410", "3"))
            favoritesList.add(Favorite("Carlos Paz", "1000-XXXX-XXXX-1285", "4"))
            favoritesList.add(Favorite("Ana Fernandez", "1000-XXXX-XXXX-7096", "5"))
            favoritesList.add(Favorite("Francisco Cuellar", "1000-XXXX-XXXX-5462", "6"))
            presenter.OnGetFavoritesSuccess(favoritesList)
        } catch (e: Exception) {
            presenter.OnProcessError("Estamos en mantenimiento, intenta nuevamente")
        }
    }
}