package com.empresa.bancamovil.Account.TransfersToOwnAcc.Activities

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.Window
import android.widget.*
import com.empresa.bancamovil.Account.TransfersToOwnAcc.Adapters.AccountsListViewAdapter
import com.empresa.bancamovil.Account.TransfersToOwnAcc.Interfaces.ITransferOwnView
import com.empresa.bancamovil.Account.TransfersToOwnAcc.Models.TransferOwn
import com.empresa.bancamovil.Account.TransfersToOwnAcc.Presenters.TransferOwnPresenter
import com.empresa.bancamovil.Products.Models.Account
import com.empresa.bancamovil.R
import com.empresa.bancamovil.Utils.BaseActivity
import com.empresa.bancamovil.Utils.Properties
import com.google.android.material.textfield.TextInputEditText
import es.dmoral.toasty.Toasty

class TransfersToOwnAccActivity : BaseActivity(), ITransferOwnView {

    private var presenter: TransferOwnPresenter? = null
    var ivLogout: ImageView? = null
    var ivBack: ImageView? = null
    var tvAcc: TextView? = null
    var tvAccAvailable: TextView? = null
    var rlAccount: RelativeLayout? = null
    var llAccount: LinearLayout? = null
    var tvAccName: TextView? = null
    var tvAccNumber: TextView? = null
    var tivAmount: TextInputEditText? = null
    var tivDetail: TextInputEditText? = null
    var btnContinue: Button? = null
    var accSession: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_transfers_to_own_acc)

        initialize()
        presenter = TransferOwnPresenter(this, applicationContext)

        ivLogout!!.setOnClickListener {
            logout()
        }

        ivBack!!.setOnClickListener {
            finish()
        }

        rlAccount!!.setOnClickListener {
            val accSession = getValueSession(Properties.ACCOUNT_NUMBER, applicationContext, Properties.SESION_PREFERENCES)
            presenter!!.getAccounts(accSession)
        }

        btnContinue!!.setOnClickListener {
            var transferOwn = TransferOwn()
            transferOwn.accName = tvAccName!!.text.toString()
            transferOwn.accNumber = tvAccNumber!!.text.toString()
            transferOwn.amount = tivAmount!!.text.toString()
            transferOwn.details = tivDetail!!.text.toString()
            presenter!!.validateData(transferOwn)
        }
    }

    //region initialize
    private fun initialize() {
        ivLogout = findViewById<ImageView>(R.id.ivLogout)
        ivBack = findViewById<ImageView>(R.id.ivBack)
        tvAcc = findViewById<TextView>(R.id.tvAcc)
        tvAccAvailable = findViewById<TextView>(R.id.tvAccAvailable)
        rlAccount = findViewById<RelativeLayout>(R.id.rlAccount)
        llAccount = findViewById<LinearLayout>(R.id.llAccount)
        tvAccName = findViewById<TextView>(R.id.tvAccName)
        tvAccNumber = findViewById<TextView>(R.id.tvAccNumber)
        tivAmount = findViewById<TextInputEditText>(R.id.tivAmount)
        tivDetail = findViewById<TextInputEditText>(R.id.tivDetail)
        btnContinue = findViewById<Button>(R.id.btnContinue)

        accSession = getValueSession(Properties.ACCOUNT_NUMBER, applicationContext, Properties.SESION_PREFERENCES)
        tvAcc!!.text = accSession
        tvAccAvailable!!.text = "$" + formatNumber(getValueSession(Properties.ACCOUNT_AVAILABLE, applicationContext, Properties.SESION_PREFERENCES))
    }
    //endregion initialize

    //region view
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        return if (keyCode == KeyEvent.KEYCODE_BACK) {
            return false
        } else super.onKeyDown(keyCode, event)
    }

    override fun OnGetAccountSuccess(accountsList: List<Account>) {
        val dialog = Dialog(this, R.style.DialogListTheme)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.list_dialog)

        var tvTitle: TextView = dialog.findViewById<TextView>(R.id.tvTitle)
        tvTitle.text = "Selecciona una cuenta"

        var btnCancel: Button = dialog.findViewById<Button>(R.id.btnCancel)

        var lvItems: ListView = dialog.findViewById<ListView>(R.id.lvItems)

        val favoritesAdapter = AccountsListViewAdapter(accountsList, applicationContext)
        lvItems.adapter = favoritesAdapter

        lvItems.setOnItemClickListener(object : AdapterView.OnItemClickListener {
            override fun onItemClick(adapter: AdapterView<*>, view: View, position: Int, arg: Long) {
                val name = view.findViewById(R.id.tvName) as TextView
                val acc = view.findViewById(R.id.tvAccount) as TextView
                tvAccName!!.text = name.text.toString()
                tvAccNumber!!.text = acc.text.toString()
                llAccount!!.visibility = View.VISIBLE
                dialog.dismiss()
            }
        })

        btnCancel.setOnClickListener {
            tvAccName!!.text = ""
            tvAccNumber!!.text = ""
            llAccount!!.visibility = View.GONE
            dialog.dismiss()
        }

        dialog.show()
    }

    override fun OnValidateData(transfer: TransferOwn) {
        val intent = Intent(this, ValidateTransferOwnActivity::class.java)
        intent.putExtra("TRANSFER", transfer)
        startActivity(intent)
    }

    override fun OnProcesError(error: String) {
        Toasty.error(applicationContext, error, Toast.LENGTH_SHORT, true).show()
    }
    //endregion view
}
