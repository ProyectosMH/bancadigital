package com.empresa.bancamovil.Account.Movements.Models

import java.io.Serializable

class AccMovement() : Serializable {

    var reference: String = ""
    var description: String = ""
    var amount: String = ""
    var type: String = ""
    var date: String = ""

    constructor(reference: String, description: String, amount: String, type: String, date: String) : this() {
        this.reference = reference
        this.description = description
        this.amount = amount
        this.type = type
        this.date = date
    }
}