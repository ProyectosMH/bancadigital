package com.empresa.bancamovil.Account.WithdrawalWithoutCard.Activities

import android.content.Intent
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.widget.*
import androidx.core.content.ContextCompat
import com.empresa.bancamovil.Account.WithdrawalWithoutCard.Interfaces.IWithoutCardView
import com.empresa.bancamovil.Account.WithdrawalWithoutCard.Models.TransferWithoutCard
import com.empresa.bancamovil.Account.WithdrawalWithoutCard.Presenters.TransferWithoutCardPresenter
import com.empresa.bancamovil.R
import com.empresa.bancamovil.Utils.BaseActivity
import com.empresa.bancamovil.Utils.Properties
import com.google.android.material.textfield.TextInputEditText
import es.dmoral.toasty.Toasty

class WithdrawalWithoutCardActivity : BaseActivity(), IWithoutCardView {

    private var presenter: TransferWithoutCardPresenter? = null
    var ivLogout: ImageView? = null
    var ivBack: ImageView? = null
    var tvAcc: TextView? = null
    var tvAccAvailable: TextView? = null
    var tv5: TextView? = null
    var tv25: TextView? = null
    var tv75: TextView? = null
    var tv100: TextView? = null
    var tv125: TextView? = null
    var tv150: TextView? = null
    var tv250: TextView? = null
    var tvOtra: TextView? = null
    var tivAmount: TextInputEditText? = null
    var llAmount: LinearLayout? = null
    var btnContinue: Button? = null
    var amount: String = ""
    var accSession: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_withdrawal_without_card)

        initialize()
        presenter = TransferWithoutCardPresenter(this, applicationContext)

        ivLogout!!.setOnClickListener {
            logout()
        }

        ivBack!!.setOnClickListener {
            finish()
        }

        tv5!!.setOnClickListener {
            selectAmount("5")
        }

        tv25!!.setOnClickListener {
            selectAmount("25")
        }

        tv75!!.setOnClickListener {
            selectAmount("75")
        }

        tv100!!.setOnClickListener {
            selectAmount("100")
        }

        tv125!!.setOnClickListener {
            selectAmount("125")
        }

        tv150!!.setOnClickListener {
            selectAmount("150")
        }

        tv250!!.setOnClickListener {
            selectAmount("250")
        }

        tvOtra!!.setOnClickListener {
            selectAmount("Otra")
        }

        btnContinue!!.setOnClickListener {
            var transferWithoutCard = TransferWithoutCard()
            transferWithoutCard.accName = getValueSession(Properties.ACCOUNT_NAME, applicationContext, Properties.SESION_PREFERENCES)
            transferWithoutCard.accNumber = accSession
            if(amount == ""){
                amount = tivAmount!!.text.toString()
            }
            transferWithoutCard.amount = amount
            presenter!!.validateData(transferWithoutCard)
        }
    }

    //region initialize
    private fun initialize() {
        ivLogout = findViewById<ImageView>(R.id.ivLogout)
        ivBack = findViewById<ImageView>(R.id.ivBack)
        tvAcc = findViewById<TextView>(R.id.tvAcc)
        tvAccAvailable = findViewById<TextView>(R.id.tvAccAvailable)
        tv5 = findViewById<TextView>(R.id.tv5)
        tv25 = findViewById<TextView>(R.id.tv25)
        tv75 = findViewById<TextView>(R.id.tv75)
        tv100 = findViewById<TextView>(R.id.tv100)
        tv125 = findViewById<TextView>(R.id.tv125)
        tv150 = findViewById<TextView>(R.id.tv150)
        tv250 = findViewById<TextView>(R.id.tv250)
        tvOtra = findViewById<TextView>(R.id.tvOtra)
        tivAmount = findViewById<TextInputEditText>(R.id.tivAmount)
        llAmount = findViewById<LinearLayout>(R.id.llAmount)
        btnContinue = findViewById<Button>(R.id.btnContinue)


        accSession = getValueSession(Properties.ACCOUNT_NUMBER, applicationContext, Properties.SESION_PREFERENCES)
        tvAcc!!.text = accSession
        tvAccAvailable!!.text = "$" + formatNumber(getValueSession(Properties.ACCOUNT_AVAILABLE, applicationContext, Properties.SESION_PREFERENCES))
    }
    //endregion initialize

    //region view
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        return if (keyCode == KeyEvent.KEYCODE_BACK) {
            return false
        } else super.onKeyDown(keyCode, event)
    }

    fun selectAmount(value: String) {
        llAmount!!.visibility = View.GONE
        amount = ""
        when (value) {
            "5" -> {
                tv5!!.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.colorPrimaryDark))
                tv5!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
                tv25!!.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
                tv25!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorPrimaryText))
                tv75!!.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
                tv75!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorPrimaryText))
                tv100!!.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
                tv100!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorPrimaryText))
                tv125!!.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
                tv125!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorPrimaryText))
                tv150!!.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
                tv150!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorPrimaryText))
                tv250!!.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
                tv250!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorPrimaryText))
                tvOtra!!.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
                tvOtra!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorPrimaryText))
                amount = tv5!!.text.toString()
            }
            "25" -> {
                tv5!!.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
                tv5!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorPrimaryText))
                tv25!!.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.colorPrimaryDark))
                tv25!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
                tv75!!.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
                tv75!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorPrimaryText))
                tv100!!.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
                tv100!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorPrimaryText))
                tv125!!.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
                tv125!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorPrimaryText))
                tv150!!.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
                tv150!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorPrimaryText))
                tv250!!.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
                tv250!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorPrimaryText))
                tvOtra!!.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
                tvOtra!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorPrimaryText))
                amount = tv25!!.text.toString()
            }
            "75" -> {
                tv5!!.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
                tv5!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorPrimaryText))
                tv25!!.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
                tv25!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorPrimaryText))
                tv75!!.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.colorPrimaryDark))
                tv75!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
                tv100!!.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
                tv100!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorPrimaryText))
                tv125!!.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
                tv125!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorPrimaryText))
                tv150!!.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
                tv150!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorPrimaryText))
                tv250!!.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
                tv250!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorPrimaryText))
                tvOtra!!.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
                tvOtra!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorPrimaryText))
                amount = tv75!!.text.toString()
            }
            "100" -> {
                tv5!!.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
                tv5!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorPrimaryText))
                tv25!!.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
                tv25!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorPrimaryText))
                tv75!!.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
                tv75!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorPrimaryText))
                tv100!!.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.colorPrimaryDark))
                tv100!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
                tv125!!.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
                tv125!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorPrimaryText))
                tv150!!.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
                tv150!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorPrimaryText))
                tv250!!.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
                tv250!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorPrimaryText))
                tvOtra!!.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
                tvOtra!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorPrimaryText))
                amount = tv100!!.text.toString()
            }
            "125" -> {
                tv5!!.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
                tv5!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorPrimaryText))
                tv25!!.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
                tv25!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorPrimaryText))
                tv75!!.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
                tv75!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorPrimaryText))
                tv100!!.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
                tv100!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorPrimaryText))
                tv125!!.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.colorPrimaryDark))
                tv125!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
                tv150!!.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
                tv150!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorPrimaryText))
                tv250!!.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
                tv250!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorPrimaryText))
                tvOtra!!.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
                tvOtra!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorPrimaryText))
                amount = tv125!!.text.toString()
            }
            "150" -> {
                tv5!!.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
                tv5!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorPrimaryText))
                tv25!!.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
                tv25!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorPrimaryText))
                tv75!!.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
                tv75!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorPrimaryText))
                tv100!!.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
                tv100!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorPrimaryText))
                tv125!!.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
                tv125!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorPrimaryText))
                tv150!!.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.colorPrimaryDark))
                tv150!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
                tv250!!.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
                tv250!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorPrimaryText))
                tvOtra!!.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
                tvOtra!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorPrimaryText))
                amount = tv150!!.text.toString()
            }
            "250" -> {
                tv5!!.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
                tv5!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorPrimaryText))
                tv25!!.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
                tv25!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorPrimaryText))
                tv75!!.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
                tv75!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorPrimaryText))
                tv100!!.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
                tv100!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorPrimaryText))
                tv125!!.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
                tv125!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorPrimaryText))
                tv150!!.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
                tv150!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorPrimaryText))
                tv250!!.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.colorPrimaryDark))
                tv250!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
                tvOtra!!.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
                tvOtra!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorPrimaryText))
                amount = tv250!!.text.toString()
            }
            "Otra" -> {//E2E2E2
                tv5!!.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
                tv5!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorPrimaryText))
                tv25!!.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
                tv25!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorPrimaryText))
                tv75!!.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
                tv75!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorPrimaryText))
                tv100!!.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
                tv100!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorPrimaryText))
                tv125!!.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
                tv125!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorPrimaryText))
                tv150!!.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
                tv150!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorPrimaryText))
                tv250!!.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
                tv250!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorPrimaryText))
                tvOtra!!.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.colorPrimaryDark))
                tvOtra!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
                llAmount!!.visibility = View.VISIBLE
            }
            else -> {
                print("x is neither")
            }
        }

    }

    override fun OnValidateData(transfer: TransferWithoutCard) {
        val intent = Intent(this, ValidateWithoutCarActivity::class.java)
        intent.putExtra("TRANSFER", transfer)
        startActivity(intent)
    }

    override fun OnProcesError(error: String) {
        Toasty.error(applicationContext, error, Toast.LENGTH_SHORT, true).show()
    }
    //endregion view
}
