package com.empresa.bancamovil.Account.ServicesPay.Activities

import android.os.Bundle
import android.view.KeyEvent
import android.widget.Button
import android.widget.TextView
import com.empresa.bancamovil.Account.ServicesPay.Models.ServicePay
import com.empresa.bancamovil.R
import com.empresa.bancamovil.Utils.BaseActivity
import java.text.SimpleDateFormat
import java.util.*

class VoucherServiPayActivity : BaseActivity() {

    var tvAcc: TextView? = null
    var tvAccNumber: TextView? = null
    var tvCodeType: TextView? = null
    var tvCode: TextView? = null
    var tvDateTime: TextView? = null
    var btnContinue: Button? = null
    lateinit var pay: ServicePay

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_voucher_servi_pay)

        initialize()

        btnContinue!!.setOnClickListener {
            goHomeProducts()
        }
    }

    //region initialize
    private fun initialize() {
        tvAcc = findViewById<TextView>(R.id.tvAcc)
        tvAccNumber = findViewById<TextView>(R.id.tvAccNumber)
        tvCodeType = findViewById<TextView>(R.id.tvCodeType)
        tvCode = findViewById<TextView>(R.id.tvCode)
        tvDateTime = findViewById<TextView>(R.id.tvDateTime)
        btnContinue = findViewById<Button>(R.id.btnContinue)

        pay = intent.extras.getSerializable("PAY") as ServicePay
        fillData()
    }
    //endregion initialize

    //region view
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        return if (keyCode == KeyEvent.KEYCODE_BACK) {
            return false
        } else super.onKeyDown(keyCode, event)
    }

    fun fillData(){
        if(pay != null){
            tvAcc!!.text = pay.accName
            tvAccNumber!!.text = pay!!.accNumber
            var des: String = pay.type
            if(pay.type == "CODEBAR"){
                des = "Código de barra"
            }
            tvCodeType!!.text = des
            tvCode!!.text = pay.code

            val sdf = SimpleDateFormat("dd/M/yyyy hh:mm:ss")
            val currentDate = sdf.format(Date())

            tvDateTime!!.text = currentDate
        }
    }
    //endregion view
}
