package com.empresa.bancamovil.Account.TransfersToThird.Models

import java.io.Serializable

class Favorite() : Serializable {

    var name: String = ""
    var account: String = ""
    var idBene: String = ""

    constructor(name: String, account: String, idBene: String) : this() {
        this.name = name
        this.account = account
        this.idBene = idBene
    }
}