package com.empresa.bancamovil.Account.TransfersToThird.Adapters

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.empresa.bancamovil.Account.TransfersToThird.Models.Favorite
import android.view.LayoutInflater
import android.widget.TextView
import com.empresa.bancamovil.R


class FavoritesListViewAdapter(favoritesList: List<Favorite>, context: Context) : BaseAdapter() {

    private val context = context
    private val favoritesList = favoritesList

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val layoutInflater :LayoutInflater = LayoutInflater.from(context)

        val view : View = layoutInflater.inflate(R.layout.item_list_view , null )
        var tvName : TextView = view.findViewById(R.id.tvName)
        var tvAccount : TextView = view.findViewById(R.id.tvAccount)

        var favorite : Favorite = favoritesList[position]

        tvName.text = favorite.name
        tvAccount.text = favorite.account

        return view
    }

    override fun getItem(position: Int): Any {
        return favoritesList[position]
    }

    override fun getItemId(position: Int): Long {
        return  0
    }

    override fun getCount(): Int {
        return favoritesList.size
    }
}