package com.empresa.bancamovil.Account.TransfersToThird.Activities

import android.os.Bundle
import android.view.KeyEvent
import android.widget.Button
import android.widget.TextView
import com.empresa.bancamovil.Account.TransfersToThird.Models.TransferThird
import com.empresa.bancamovil.R
import com.empresa.bancamovil.Utils.BaseActivity
import com.empresa.bancamovil.Utils.Properties
import java.text.SimpleDateFormat
import java.util.*

class VoucherThirdActivity : BaseActivity() {

    var tvOriginAcc: TextView? = null
    var tvNameBene: TextView? = null
    var tvDestiAcc: TextView? = null
    var tvAmount: TextView? = null
    var tvDetails: TextView? = null
    var tvDateTime: TextView? = null
    var btnContinue: Button? = null
    lateinit var transfer: TransferThird

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_voucher_third)

        initialize()

        btnContinue!!.setOnClickListener {
            goHomeProducts()
        }
    }

    //region initialize
    private fun initialize() {
        tvOriginAcc = findViewById<TextView>(R.id.tvOriginAcc)
        tvNameBene = findViewById<TextView>(R.id.tvNameBene)
        tvDestiAcc = findViewById<TextView>(R.id.tvDestiAcc)
        tvAmount = findViewById<TextView>(R.id.tvAmount)
        tvDetails = findViewById<TextView>(R.id.tvDetails)
        tvDateTime = findViewById<TextView>(R.id.tvDateTime)
        btnContinue = findViewById<Button>(R.id.btnContinue)

        transfer = intent.extras.getSerializable("TRANSFER") as TransferThird
        fillData()
    }
    //endregion initialize

    //region view
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        return if (keyCode == KeyEvent.KEYCODE_BACK) {
            return false
        } else super.onKeyDown(keyCode, event)
    }

    fun fillData(){
        if(transfer != null){
            val accSession = getValueSession(Properties.ACCOUNT_NUMBER, applicationContext, Properties.SESION_PREFERENCES)
            tvOriginAcc!!.text = accSession
            tvNameBene!!.text = transfer!!.favName
            tvDestiAcc!!.text = transfer!!.favAcc
            tvAmount!!.text = "$" + formatNumber(transfer!!.amount)
            tvDetails!!.text = transfer!!.details

            val sdf = SimpleDateFormat("dd/M/yyyy hh:mm:ss")
            val currentDate = sdf.format(Date())

            tvDateTime!!.text = currentDate
        }
    }
    //endregion view
}
