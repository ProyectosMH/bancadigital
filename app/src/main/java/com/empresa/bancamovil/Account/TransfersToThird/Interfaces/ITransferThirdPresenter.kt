package com.empresa.bancamovil.Account.TransfersToThird.Interfaces

import com.empresa.bancamovil.Account.TransfersToThird.Models.Favorite
import com.empresa.bancamovil.Account.TransfersToThird.Models.TransferThird

interface ITransferThirdPresenter {
    fun getFavorites()
    fun validateData(transfer: TransferThird)
    fun validateTransfer(transfer: TransferThird)

    fun OnGetFavoritesSuccess(favoritesList: List<Favorite>)
    fun OnProcessError(error: String)
}