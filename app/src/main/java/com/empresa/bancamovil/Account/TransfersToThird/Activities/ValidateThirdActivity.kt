package com.empresa.bancamovil.Account.TransfersToThird.Activities

import android.content.Intent
import android.os.Bundle
import android.view.KeyEvent
import android.widget.Button
import android.widget.Toast
import com.empresa.bancamovil.Account.TransfersToThird.Interfaces.ITransferThirdView
import com.empresa.bancamovil.Account.TransfersToThird.Models.Favorite
import com.empresa.bancamovil.Account.TransfersToThird.Presenters.TransferThirdPresenter
import com.empresa.bancamovil.R
import com.empresa.bancamovil.Utils.BaseActivity
import es.dmoral.toasty.Toasty
import android.widget.ImageView
import android.widget.TextView
import com.empresa.bancamovil.Account.TransfersToThird.Models.TransferThird
import com.empresa.bancamovil.Utils.Properties

class ValidateThirdActivity : BaseActivity(), ITransferThirdView {

    private var presenter: TransferThirdPresenter? = null
    var ivLogout: ImageView? = null
    var ivBack: ImageView? = null
    var tvOriginAcc: TextView? = null
    var tvNameBene: TextView? = null
    var tvDestiAcc: TextView? = null
    var tvAmount: TextView? = null
    var tvDetails: TextView? = null
    var btnContinue: Button? = null
    lateinit var transfer: TransferThird

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_validate_third)

        initialize()
        presenter = TransferThirdPresenter(this, applicationContext)

        ivLogout!!.setOnClickListener {
            logout()
        }

        ivBack!!.setOnClickListener {
            finish()
        }

        btnContinue!!.setOnClickListener {
            presenter!!.validateTransfer(transfer)
        }
    }

    //region initialize
    private fun initialize() {
        ivLogout = findViewById<ImageView>(R.id.ivLogout)
        ivBack = findViewById<ImageView>(R.id.ivBack)
        tvOriginAcc = findViewById<TextView>(R.id.tvOriginAcc)
        tvNameBene = findViewById<TextView>(R.id.tvNameBene)
        tvDestiAcc = findViewById<TextView>(R.id.tvDestiAcc)
        tvAmount = findViewById<TextView>(R.id.tvAmount)
        tvDetails = findViewById<TextView>(R.id.tvDetails)
        btnContinue = findViewById<Button>(R.id.btnContinue)

        transfer = intent.extras.getSerializable("TRANSFER") as TransferThird
        fillData()
    }
    //endregion initialize

    //region view
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        return if (keyCode == KeyEvent.KEYCODE_BACK) {
            return false
        } else super.onKeyDown(keyCode, event)
    }

    fun fillData(){
        if(transfer != null){
            val accSession = getValueSession(Properties.ACCOUNT_NUMBER, applicationContext, Properties.SESION_PREFERENCES)
            tvOriginAcc!!.text = accSession
            tvNameBene!!.text = transfer!!.favName
            tvDestiAcc!!.text = transfer!!.favAcc
            tvAmount!!.text = "$" + formatNumber(transfer!!.amount)
            tvDetails!!.text = transfer!!.details
        }
    }

    override fun OnGetFavoritesSuccess(favoritesList: List<Favorite>) {

    }

    override fun OnValidateData(transfer: TransferThird) {
        val intent = Intent(this, VoucherThirdActivity::class.java)
        intent.putExtra("TRANSFER", transfer)
        startActivity(intent)
    }

    override fun OnProcesError(error: String) {
        Toasty.error(applicationContext, error, Toast.LENGTH_SHORT, true).show()
    }
    //endregion view
}
