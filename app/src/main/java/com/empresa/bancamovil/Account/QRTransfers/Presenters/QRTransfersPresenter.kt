package com.empresa.bancamovil.Account.QRTransfers.Presenters

import android.content.Context
import com.empresa.bancamovil.Account.QRTransfers.Interfaces.IQRTransfersPresenter
import com.empresa.bancamovil.Account.QRTransfers.Interfaces.IQRTransfersView
import com.empresa.bancamovil.Account.QRTransfers.Models.QRTransfer

public class QRTransfersPresenter(iQRTransfersView: IQRTransfersView, context: Context) :
    IQRTransfersPresenter {

    private var iQRTransfersView = iQRTransfersView
    private var context = context

    override fun validateData(transfer: QRTransfer) {
        if(transfer.accName == ""){
            iQRTransfersView!!.OnProcesError("Por favor selecciona una cuenta")
        }else if(transfer.amount == ""){
            iQRTransfersView!!.OnProcesError("Ingresa el monto de la transferencia")
        }else if(transfer.details == ""){
            iQRTransfersView!!.OnProcesError("Ingresa el detalle de la transferencia")
        }else{
            iQRTransfersView.OnValidateData(transfer)
        }
    }

    override fun validateTransfer(transfer: QRTransfer) {
        iQRTransfersView.OnValidateData(transfer)
    }

    override fun OnProcessError(error: String) {
        iQRTransfersView!!.OnProcesError(error)
    }
}