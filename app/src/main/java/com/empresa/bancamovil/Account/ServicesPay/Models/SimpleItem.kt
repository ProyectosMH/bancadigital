package com.empresa.bancamovil.Account.ServicesPay.Models

import java.io.Serializable

class SimpleItem() : Serializable {

    var description: String = ""
    var value: String = ""

    constructor(description: String, value: String) : this() {
        this.description = description
        this.value = value
    }
}