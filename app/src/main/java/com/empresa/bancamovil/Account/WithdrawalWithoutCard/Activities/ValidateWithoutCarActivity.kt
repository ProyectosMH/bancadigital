package com.empresa.bancamovil.Account.WithdrawalWithoutCard.Activities

import android.content.Intent
import android.os.Bundle
import android.view.KeyEvent
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.empresa.bancamovil.Account.WithdrawalWithoutCard.Interfaces.IWithoutCardView
import com.empresa.bancamovil.Account.WithdrawalWithoutCard.Models.TransferWithoutCard
import com.empresa.bancamovil.Account.WithdrawalWithoutCard.Presenters.TransferWithoutCardPresenter
import com.empresa.bancamovil.R
import com.empresa.bancamovil.Utils.BaseActivity
import es.dmoral.toasty.Toasty

class ValidateWithoutCarActivity : BaseActivity(), IWithoutCardView {

    private var presenter: TransferWithoutCardPresenter? = null
    var ivLogout: ImageView? = null
    var ivBack: ImageView? = null
    var tvNameAcc: TextView? = null
    var tvAccNumber: TextView? = null
    var tvAmount: TextView? = null
    var btnContinue: Button? = null
    var transfer = TransferWithoutCard()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_validate_without_car)

        initialize()
        presenter = TransferWithoutCardPresenter(this, applicationContext)

        ivLogout!!.setOnClickListener {
            logout()
        }

        ivBack!!.setOnClickListener {
            finish()
        }

        btnContinue!!.setOnClickListener {
            presenter!!.validateTransfer(transfer)
        }
    }

    //region initialize
    private fun initialize() {
        ivLogout = findViewById<ImageView>(R.id.ivLogout)
        ivBack = findViewById<ImageView>(R.id.ivBack)
        tvNameAcc = findViewById<TextView>(R.id.tvNameAcc)
        tvAccNumber = findViewById<TextView>(R.id.tvAccNumber)
        tvAmount = findViewById<TextView>(R.id.tvAmount)
        btnContinue = findViewById<Button>(R.id.btnContinue)

        transfer = intent.extras.getSerializable("TRANSFER") as TransferWithoutCard
        fillData()
    }
    //endregion initialize

    //region view
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        return if (keyCode == KeyEvent.KEYCODE_BACK) {
            return false
        } else super.onKeyDown(keyCode, event)
    }

    fun fillData(){
        if(transfer != null){
            tvNameAcc!!.text = transfer!!.accName
            tvAccNumber!!.text = transfer!!.accNumber
            tvAmount!!.text = "$" + formatNumber(transfer!!.amount)
        }
    }

    override fun OnValidateData(transfer: TransferWithoutCard) {
        val intent = Intent(this, VoucherWithoutCarActivity::class.java)
        intent.putExtra("TRANSFER", transfer)
        startActivity(intent)
    }

    override fun OnProcesError(error: String) {
        Toasty.error(applicationContext, error, Toast.LENGTH_SHORT, true).show()
    }
    //endregion view
}
