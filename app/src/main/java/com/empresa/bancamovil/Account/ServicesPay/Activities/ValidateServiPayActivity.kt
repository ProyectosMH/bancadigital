package com.empresa.bancamovil.Account.ServicesPay.Activities

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.empresa.bancamovil.Account.ServicesPay.Interfaces.IServiPayView
import com.empresa.bancamovil.Account.ServicesPay.Models.ServicePay
import com.empresa.bancamovil.Account.ServicesPay.Presenters.ServicePayPresenter
import com.empresa.bancamovil.R
import com.empresa.bancamovil.Utils.BaseActivity
import es.dmoral.toasty.Toasty

class ValidateServiPayActivity : BaseActivity(), IServiPayView {

    private var presenter: ServicePayPresenter? = null
    var ivLogout: ImageView? = null
    var ivBack: ImageView? = null
    var tvNameAcc: TextView? = null
    var tvAccNumber: TextView? = null
    var tvDesCode: TextView? = null
    var tvCode: TextView? = null
    var btnContinue: Button? = null
    lateinit var pay: ServicePay

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_validate_servi_pay)

        initialize()
        presenter = ServicePayPresenter(this, applicationContext)

        ivLogout!!.setOnClickListener {
            logout()
        }

        ivBack!!.setOnClickListener {
            finish()
        }

        btnContinue!!.setOnClickListener {
            presenter!!.validatePay(pay)
        }
    }

    //region initialize
    private fun initialize() {
        ivLogout = findViewById<ImageView>(R.id.ivLogout)
        ivBack = findViewById<ImageView>(R.id.ivBack)
        tvNameAcc = findViewById<TextView>(R.id.tvNameAcc)
        tvAccNumber = findViewById<TextView>(R.id.tvAccNumber)
        tvDesCode = findViewById<TextView>(R.id.tvDesCode)
        tvCode = findViewById<TextView>(R.id.tvCode)
        btnContinue = findViewById<Button>(R.id.btnContinue)

        pay = intent.extras.getSerializable("PAY") as ServicePay
        fillData()
    }
    //endregion initialize

    //region view
    fun fillData(){
        if(pay != null){
            tvNameAcc!!.text = pay.accName
            tvAccNumber!!.text = pay.accNumber
            var des: String = pay.type
            if(pay.type == "CODEBAR"){
                des = "Código de barra"
            }
            tvDesCode!!.text = des
            tvCode!!.text = pay.code
        }
    }

    override fun OnValidateData(pay: ServicePay) {
        val intent = Intent(this, VoucherServiPayActivity::class.java)
        intent.putExtra("PAY", pay)
        startActivity(intent)
    }

    override fun OnProcesError(error: String) {
        Toasty.error(applicationContext, error, Toast.LENGTH_SHORT, true).show()
    }
    //endregion view
}
