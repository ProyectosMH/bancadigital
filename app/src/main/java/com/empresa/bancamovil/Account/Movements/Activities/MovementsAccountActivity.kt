package com.empresa.bancamovil.Account.Movements.Activities

import android.os.Bundle
import android.view.KeyEvent
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.empresa.bancamovil.Account.Movements.Adapters.AccMovementsAdapter
import com.empresa.bancamovil.Account.Movements.Interfaces.IAccMovementsView
import com.empresa.bancamovil.Account.Movements.Models.AccMovement
import com.empresa.bancamovil.Account.Movements.Presenters.AccMovementsPresenter
import com.empresa.bancamovil.R
import com.empresa.bancamovil.Utils.BaseActivity
import com.empresa.bancamovil.Utils.Properties
import es.dmoral.toasty.Toasty

class MovementsAccountActivity : BaseActivity(), IAccMovementsView {

    private var presenter: AccMovementsPresenter? = null
    var ivLogout: ImageView? = null
    var ivBack: ImageView? = null
    var tvAcc: TextView? = null
    var tvAccAvailable: TextView? = null
    var rvMovements: RecyclerView? = null
    var accSession: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_account_movements)

        initialize()
        presenter = AccMovementsPresenter(this, applicationContext)

        ivLogout!!.setOnClickListener {
            logout()
        }

        ivBack!!.setOnClickListener {
            finish()
        }

        presenter!!.getAccMovements()
    }

    //region initialize
    private fun initialize() {
        ivLogout = findViewById<ImageView>(R.id.ivLogout)
        ivBack = findViewById<ImageView>(R.id.ivBack)
        tvAcc = findViewById<TextView>(R.id.tvAcc)
        tvAccAvailable = findViewById<TextView>(R.id.tvAccAvailable)
        rvMovements = findViewById<RecyclerView>(R.id.rvMovements)

        accSession = getValueSession(Properties.ACCOUNT_NUMBER, applicationContext, Properties.SESION_PREFERENCES)
        tvAcc!!.text = accSession
        tvAccAvailable!!.text = "$" + formatNumber(getValueSession(Properties.ACCOUNT_AVAILABLE, applicationContext, Properties.SESION_PREFERENCES))
    }
    //endregion initialize

    //region view
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        return if (keyCode == KeyEvent.KEYCODE_BACK) {
            return false
        } else super.onKeyDown(keyCode, event)
    }

    override fun OnGetAccMovementsSuccess(movementsList: List<AccMovement>) {
        val adapter = AccMovementsAdapter(movementsList, this)
        rvMovements!!.layoutManager = LinearLayoutManager(applicationContext)
        rvMovements!!.adapter = adapter
    }

    override fun OnProcesError(error: String) {
        Toasty.error(applicationContext, error, Toast.LENGTH_SHORT, true).show()
    }
    //endregion view
}
