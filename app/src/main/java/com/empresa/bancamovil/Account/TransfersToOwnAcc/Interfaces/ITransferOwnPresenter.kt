package com.empresa.bancamovil.Account.TransfersToOwnAcc.Interfaces

import com.empresa.bancamovil.Account.TransfersToOwnAcc.Models.TransferOwn
import com.empresa.bancamovil.Products.Models.Account

interface ITransferOwnPresenter {
    fun getAccounts(accSession: String)
    fun validateData(transfer: TransferOwn)
    fun validateTransfer(transfer: TransferOwn)

    fun OnGetAccountsSuccess(accountsList: List<Account>)
    fun OnProcessError(error: String)
}