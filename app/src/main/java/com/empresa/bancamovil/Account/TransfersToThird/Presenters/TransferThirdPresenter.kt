package com.empresa.bancamovil.Account.TransfersToThird.Presenters

import android.content.Context
import com.empresa.bancamovil.Account.TransfersToThird.Interfaces.ITransferThirdPresenter
import com.empresa.bancamovil.Account.TransfersToThird.Interfaces.ITransferThirdView
import com.empresa.bancamovil.Account.TransfersToThird.Models.Favorite
import com.empresa.bancamovil.Account.TransfersToThird.Models.TransferThird
import com.empresa.bancamovil.Account.TransfersToThird.Repository.TransferThirdRepository

public class TransferThirdPresenter(iTransferThirdView: ITransferThirdView, context: Context) : ITransferThirdPresenter {

    private var iTransferThirdView = iTransferThirdView
    private var context = context
    private var repository = TransferThirdRepository()

    override fun getFavorites() {
        repository.getFavorites(this)
    }

    override fun validateData(transfer: TransferThird) {
        if(transfer.favName == ""){
            iTransferThirdView!!.OnProcesError("Por favor selecciona una cuenta a acreditar")
        }else if(transfer.amount == ""){
            iTransferThirdView!!.OnProcesError("Ingresa el monto de la transferencia")
        }else if(transfer.details == ""){
            iTransferThirdView!!.OnProcesError("Ingresa el detalle de la transferencia")
        }else{
            iTransferThirdView.OnValidateData(transfer)
        }
    }

    override fun validateTransfer(transfer: TransferThird) {
        iTransferThirdView.OnValidateData(transfer)
    }

    override fun OnGetFavoritesSuccess(favoritesList: List<Favorite>) {
        iTransferThirdView!!.OnGetFavoritesSuccess(favoritesList)
    }

    override fun OnProcessError(error: String) {
        iTransferThirdView!!.OnProcesError(error)
    }
}