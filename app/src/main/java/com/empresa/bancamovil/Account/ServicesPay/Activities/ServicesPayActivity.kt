package com.empresa.bancamovil.Account.ServicesPay.Activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.KeyEvent
import com.dev.materialspinner.MaterialSpinner
import com.empresa.bancamovil.R
import com.empresa.bancamovil.Utils.BaseActivity
import android.view.View
import android.widget.*
import com.empresa.bancamovil.Account.ServicesPay.Interfaces.IServiPayView
import com.empresa.bancamovil.Account.ServicesPay.Models.ServicePay
import com.empresa.bancamovil.Account.ServicesPay.Presenters.ServicePayPresenter
import com.google.zxing.integration.android.IntentIntegrator
import es.dmoral.toasty.Toasty
import android.text.Editable
import android.text.TextWatcher
import android.view.inputmethod.InputMethodManager
import androidx.core.view.isVisible
import android.widget.Toast
import com.empresa.bancamovil.Utils.Properties

class ServicesPayActivity : BaseActivity(), IServiPayView, AdapterView.OnItemSelectedListener {

    private var presenter: ServicePayPresenter? = null
    var ivLogout: ImageView? = null
    var ivBack: ImageView? = null
    var llNPE: LinearLayout? = null
    var llCodeBar: LinearLayout? = null
    var ivCodeBar: ImageView? = null
    var tvAcc: TextView? = null
    var tvAccAvailable: TextView? = null
    var tvCodeBar: TextView? = null
    var etNpe1: EditText? = null
    var etNpe2: EditText? = null
    var etNpe3: EditText? = null
    var etNpe4: EditText? = null
    var etNpe5: EditText? = null
    var etNpe6: EditText? = null
    var etNpe7: EditText? = null
    var etNpe8: EditText? = null
    var etNpe9: EditText? = null
    var etNpe10: EditText? = null
    var btnContinue: Button? = null
    private lateinit var msMetodo : MaterialSpinner
    var accSession: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_services_pay)

        initialize()
        presenter = ServicePayPresenter(this, applicationContext)

        ivLogout!!.setOnClickListener {
            logout()
        }

        ivBack!!.setOnClickListener {
            finish()
        }

        ivCodeBar!!.setOnClickListener {
            scanCodeBar()
        }

        tvCodeBar!!.setOnClickListener {
            scanCodeBar()
        }

        etNpe1!!.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {}

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                longitudTextET(s.length, "et1")
            }
        })

        etNpe2!!.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {}

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                longitudTextET(s.length, "et2")
            }
        })

        etNpe3!!.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {}

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                longitudTextET(s.length, "et3")
            }
        })

        etNpe4!!.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {}

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                longitudTextET(s.length, "et4")
            }
        })

        etNpe5!!.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {}

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                longitudTextET(s.length, "et5")
            }
        })

        etNpe6!!.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {}

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                longitudTextET(s.length, "et6")
            }
        })

        etNpe7!!.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {}

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                longitudTextET(s.length, "et7")
            }
        })

        etNpe8!!.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {}

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                longitudTextET(s.length, "et8")
            }
        })

        etNpe9!!.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {}

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                longitudTextET(s.length, "et9")
            }
        })

        etNpe10!!.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {}

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                longitudTextET(s.length, "et10")
            }
        })

        btnContinue!!.setOnClickListener {
            var servicePay = ServicePay()
            servicePay.accName = getValueSession(Properties.ACCOUNT_NAME, applicationContext, Properties.SESION_PREFERENCES)
            servicePay.accNumber = accSession
            servicePay.code = etNpe1!!.text.toString() + "" + etNpe2!!.text.toString() + "" + etNpe3!!.text.toString() + "" + etNpe4!!.text.toString() + "" + etNpe5!!.text.toString() + "" + etNpe6!!.text.toString() + "" + etNpe7!!.text.toString() + "" + etNpe8!!.text.toString() + "" + etNpe9!!.text.toString() + "" + etNpe10!!.text.toString()
            servicePay.type = "NPE"
            presenter!!.validateData(servicePay)
        }
    }

    //region initialize
    private fun initialize() {
        ivLogout = findViewById<ImageView>(R.id.ivLogout)
        ivBack = findViewById<ImageView>(R.id.ivBack)
        msMetodo = findViewById<MaterialSpinner>(R.id.msMetodo)
        llNPE = findViewById<LinearLayout>(R.id.llNPE)
        llCodeBar = findViewById<LinearLayout>(R.id.llCodeBar)
        ivCodeBar = findViewById<ImageView>(R.id.ivCodeBar)
        tvAcc = findViewById<TextView>(R.id.tvAcc)
        tvAccAvailable = findViewById<TextView>(R.id.tvAccAvailable)
        tvCodeBar = findViewById<TextView>(R.id.tvCodeBar)
        etNpe1 = findViewById<EditText>(R.id.etNpe1)
        etNpe2 = findViewById<EditText>(R.id.etNpe2)
        etNpe3 = findViewById<EditText>(R.id.etNpe3)
        etNpe4 = findViewById<EditText>(R.id.etNpe4)
        etNpe5 = findViewById<EditText>(R.id.etNpe5)
        etNpe6 = findViewById<EditText>(R.id.etNpe6)
        etNpe7 = findViewById<EditText>(R.id.etNpe7)
        etNpe8 = findViewById<EditText>(R.id.etNpe8)
        etNpe9 = findViewById<EditText>(R.id.etNpe9)
        etNpe10 = findViewById<EditText>(R.id.etNpe10)
        btnContinue = findViewById<Button>(R.id.btnContinue)
        msMetodo.getSpinner().onItemSelectedListener = this

        accSession = getValueSession(Properties.ACCOUNT_NUMBER, applicationContext, Properties.SESION_PREFERENCES)
        tvAcc!!.text = accSession
        tvAccAvailable!!.text = "$" + formatNumber(getValueSession(Properties.ACCOUNT_AVAILABLE, applicationContext, Properties.SESION_PREFERENCES))
        fillSpiner()
    }
    //endregion initialize

    //region view
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if(llNPE!!.isVisible){
            findet()
        }
        return if (keyCode == KeyEvent.KEYCODE_BACK) {
            return false
        } else super.onKeyDown(keyCode, event)
    }

    fun findet(){
        if(etNpe1!!.hasFocus()){
            var value: String = etNpe1!!.text.toString()
            longitudTextET(value.length, "et1")
        }else if(etNpe2!!.hasFocus()){
            var value: String = etNpe2!!.text.toString()
            longitudTextET(value.length, "et2")
        }else if(etNpe3!!.hasFocus()){
            var value: String = etNpe3!!.text.toString()
            longitudTextET(value.length, "et3")
        }else if(etNpe4!!.hasFocus()){
            var value: String = etNpe4!!.text.toString()
            longitudTextET(value.length, "et4")
        }else if(etNpe5!!.hasFocus()){
            var value: String = etNpe5!!.text.toString()
            longitudTextET(value.length, "et5")
        }else if(etNpe6!!.hasFocus()){
            var value: String = etNpe6!!.text.toString()
            longitudTextET(value.length, "et6")
        }else if(etNpe7!!.hasFocus()){
            var value: String = etNpe7!!.text.toString()
            longitudTextET(value.length, "et7")
        }else if(etNpe8!!.hasFocus()){
            var value: String = etNpe8!!.text.toString()
            longitudTextET(value.length, "et8")
        }else if(etNpe9!!.hasFocus()){
            var value: String = etNpe9!!.text.toString()
            longitudTextET(value.length, "et9")
        }else if(etNpe10!!.hasFocus()){
            var value: String = etNpe10!!.text.toString()
            longitudTextET(value.length, "et10")
        }
    }

    fun fillSpiner(){
        var list_of_items = arrayOf("Selecciona el metodo de pago","NPE", "Código de barra")
        val aa = ArrayAdapter(this, android.R.layout.simple_spinner_item, list_of_items)
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        msMetodo!!.setAdapter(aa)
    }

    override fun onItemSelected(arg0: AdapterView<*>, arg1: View, position: Int, id: Long) {
        if(position == 0)
        {
            llNPE!!.visibility = View.GONE
            llCodeBar!!.visibility = View.GONE
            //msMetodo.setError("Please select Country")
        }
        else
        {
            msMetodo.setErrorEnabled(false)
            msMetodo.setLabel("Metodo seleccionado")
            if(position == 1){
                llNPE!!.visibility = View.VISIBLE
                llCodeBar!!.visibility = View.GONE
            }else if(position == 2){
                llNPE!!.visibility = View.GONE
                llCodeBar!!.visibility = View.VISIBLE
            }
        }
    }

    override fun onNothingSelected(arg0: AdapterView<*>) {
    }

    fun scanCodeBar() {
        val intentIntegrator = IntentIntegrator(this)
        intentIntegrator.setBeepEnabled(false)
        intentIntegrator.setCameraId(0)
        intentIntegrator.setPrompt("Escaneando código")
        intentIntegrator.setBarcodeImageEnabled(false)
        intentIntegrator.initiateScan()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
        if (result != null) {
            if (result.contents == null) {
                Toast.makeText(this, "Escaneo cancelado", Toast.LENGTH_SHORT).show()
            } else {
                var servicePay = ServicePay()
                servicePay.accName = getValueSession(Properties.ACCOUNT_NAME, applicationContext, Properties.SESION_PREFERENCES)
                servicePay.accNumber = accSession
                servicePay.code = result.contents
                servicePay.type = "CODEBAR"
                presenter!!.validateData(servicePay)
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    fun longitudTextET(text: Int, id: String) {
        if (text == 4) {
            pasarEditText(id)
        }else if (text == 5) {
            pasarEditText(id)
        } else if (text == 0) {
            pasarEditTextAnterior(id)
        }
    }

    fun pasarEditText(id: String) {
        val manager =
            applicationContext.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        when (id) {
            "et1" -> {
                etNpe2!!.requestFocus()
                manager.showSoftInput(etNpe2, InputMethodManager.SHOW_IMPLICIT)
            }
            "et2" -> {
                etNpe3!!.requestFocus()
                manager.showSoftInput(etNpe3, InputMethodManager.SHOW_IMPLICIT)
            }
            "et3" -> {
                etNpe4!!.requestFocus()
                manager.showSoftInput(etNpe4, InputMethodManager.SHOW_IMPLICIT)
            }
            "et4" -> {
                etNpe5!!.requestFocus()
                manager.showSoftInput(etNpe5, InputMethodManager.SHOW_IMPLICIT)
            }
            "et5" -> {
                etNpe6!!.requestFocus()
                manager.showSoftInput(etNpe6, InputMethodManager.SHOW_IMPLICIT)
            }
            "et6" -> {
                etNpe7!!.requestFocus()
                manager.showSoftInput(etNpe7, InputMethodManager.SHOW_IMPLICIT)
            }
            "et7" -> {
                etNpe8!!.requestFocus()
                manager.showSoftInput(etNpe8, InputMethodManager.SHOW_IMPLICIT)
            }
            "et8" -> {
                etNpe9!!.requestFocus()
                manager.showSoftInput(etNpe9, InputMethodManager.SHOW_IMPLICIT)
            }
            "et9" -> {
                etNpe10!!.requestFocus()
                manager.showSoftInput(etNpe10, InputMethodManager.SHOW_IMPLICIT)
            }
            else -> {
                etNpe10!!.requestFocus()
                val imm =
                    applicationContext.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(etNpe10!!.windowToken, 0)
            }
        }
    }

    fun pasarEditTextAnterior(id: String) {
        val manager =
            applicationContext.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        when (id) {
            "et2" -> {
                etNpe1!!.requestFocus()
                manager.showSoftInput(etNpe1, InputMethodManager.SHOW_IMPLICIT)
            }
            "et3" -> {
                etNpe2!!.requestFocus()
                manager.showSoftInput(etNpe2, InputMethodManager.SHOW_IMPLICIT)
            }
            "et4" -> {
                etNpe3!!.requestFocus()
                manager.showSoftInput(etNpe3, InputMethodManager.SHOW_IMPLICIT)
            }
            "et5" -> {
                etNpe4!!.requestFocus()
                manager.showSoftInput(etNpe4, InputMethodManager.SHOW_IMPLICIT)
            }
            "et6" -> {
                etNpe5!!.requestFocus()
                manager.showSoftInput(etNpe5, InputMethodManager.SHOW_IMPLICIT)
            }
            "et7" -> {
                etNpe6!!.requestFocus()
                manager.showSoftInput(etNpe6, InputMethodManager.SHOW_IMPLICIT)
            }
            "et8" -> {
                etNpe7!!.requestFocus()
                manager.showSoftInput(etNpe7, InputMethodManager.SHOW_IMPLICIT)
            }
            "et9" -> {
                etNpe8!!.requestFocus()
                manager.showSoftInput(etNpe8, InputMethodManager.SHOW_IMPLICIT)
            }
            "et10" -> {
                etNpe9!!.requestFocus()
                manager.showSoftInput(etNpe9, InputMethodManager.SHOW_IMPLICIT)
            }
            else -> {
            }
        }
    }

    override fun OnValidateData(pay: ServicePay) {
        val intent = Intent(this, ValidateServiPayActivity::class.java)
        intent.putExtra("PAY", pay)
        startActivity(intent)
    }

    override fun OnProcesError(error: String) {
        Toasty.error(applicationContext, error, Toast.LENGTH_SHORT, true).show()
    }
    //endregion view
}
