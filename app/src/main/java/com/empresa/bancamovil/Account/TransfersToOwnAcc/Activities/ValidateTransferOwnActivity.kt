package com.empresa.bancamovil.Account.TransfersToOwnAcc.Activities

import android.content.Intent
import android.os.Bundle
import android.view.KeyEvent
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.empresa.bancamovil.Account.TransfersToOwnAcc.Interfaces.ITransferOwnView
import com.empresa.bancamovil.Account.TransfersToOwnAcc.Models.TransferOwn
import com.empresa.bancamovil.Account.TransfersToOwnAcc.Presenters.TransferOwnPresenter
import com.empresa.bancamovil.Products.Models.Account
import com.empresa.bancamovil.R
import com.empresa.bancamovil.Utils.BaseActivity
import com.empresa.bancamovil.Utils.Properties
import es.dmoral.toasty.Toasty
import kotlinx.android.synthetic.main.layout_progress_bar.*

class ValidateTransferOwnActivity : BaseActivity(), ITransferOwnView {

    private var presenter: TransferOwnPresenter? = null
    var ivLogout: ImageView? = null
    var ivBack: ImageView? = null
    var tvOriginAcc: TextView? = null
    var tvNameAcc: TextView? = null
    var tvDestiAcc: TextView? = null
    var tvAmount: TextView? = null
    var tvDetails: TextView? = null
    var btnContinue: Button? = null
    lateinit var transfer: TransferOwn

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_validate_transfer_own)

        initialize()
        presenter = TransferOwnPresenter(this, applicationContext)

        ivLogout!!.setOnClickListener {
            logout()
        }

        ivBack!!.setOnClickListener {
            finish()
        }

        btnContinue!!.setOnClickListener {
            presenter!!.validateTransfer(transfer)
        }
    }

    //region initialize
    private fun initialize() {
        ivLogout = findViewById<ImageView>(R.id.ivLogout)
        ivBack = findViewById<ImageView>(R.id.ivBack)
        tvOriginAcc = findViewById<TextView>(R.id.tvOriginAcc)
        tvNameAcc = findViewById<TextView>(R.id.tvNameAcc)
        tvDestiAcc = findViewById<TextView>(R.id.tvDestiAcc)
        tvAmount = findViewById<TextView>(R.id.tvAmount)
        tvDetails = findViewById<TextView>(R.id.tvDetails)
        btnContinue = findViewById<Button>(R.id.btnContinue)

        transfer = intent.extras.getSerializable("TRANSFER") as TransferOwn
        fillData()
    }
    //endregion initialize

    //region view
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        return if (keyCode == KeyEvent.KEYCODE_BACK) {
            return false
        } else super.onKeyDown(keyCode, event)
    }

    fun fillData(){
        if(transfer != null){
            val accSession = getValueSession(Properties.ACCOUNT_NUMBER, applicationContext, Properties.SESION_PREFERENCES)
            tvOriginAcc!!.text = accSession
            tvNameAcc!!.text = transfer!!.accName
            tvDestiAcc!!.text = transfer!!.accNumber
            tvAmount!!.text = "$" + formatNumber(transfer!!.amount)
            tvDetails!!.text = transfer!!.details
        }
    }

    override fun OnGetAccountSuccess(accountsList: List<Account>) {

    }

    override fun OnValidateData(transfer: TransferOwn) {
        val intent = Intent(this, VoucherTransferOwnActivity::class.java)
        intent.putExtra("TRANSFER", transfer)
        startActivity(intent)
    }

    override fun OnProcesError(error: String) {
        Toasty.error(applicationContext, error, Toast.LENGTH_SHORT, true).show()
    }
    //endregion view
}
