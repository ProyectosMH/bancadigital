package com.empresa.bancamovil.Account.TransfersToThird.Models

import java.io.Serializable

class TransferThird() : Serializable {

    var favName: String = ""
    var favAcc: String = ""
    var amount: String = ""
    var details: String = ""

    constructor(favName: String, favAcc: String, amount: String, details: String) : this() {
        this.favName = favName
        this.favAcc = favAcc
        this.amount = amount
        this.details= details
    }
}