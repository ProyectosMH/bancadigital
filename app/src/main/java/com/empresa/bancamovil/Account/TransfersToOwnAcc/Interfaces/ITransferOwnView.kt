package com.empresa.bancamovil.Account.TransfersToOwnAcc.Interfaces

import com.empresa.bancamovil.Account.TransfersToOwnAcc.Models.TransferOwn
import com.empresa.bancamovil.Products.Models.Account

interface ITransferOwnView {
    fun OnGetAccountSuccess(accountsList: List<Account>)
    fun OnValidateData(transfer: TransferOwn)
    fun OnProcesError(error: String)
}