package com.empresa.bancamovil.Account.TransfersToOwnAcc.Models

import java.io.Serializable

class TransferOwn() : Serializable {

    var accName: String = ""
    var accNumber: String = ""
    var amount: String = ""
    var details: String = ""

    constructor(accName: String, accNumber: String, amount: String, details: String) : this() {
        this.accName = accName
        this.accNumber = accNumber
        this.amount = amount
        this.details= details
    }
}