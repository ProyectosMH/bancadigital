package com.empresa.bancamovil.Account.QRTransfers.Activities

import android.os.Bundle
import android.view.KeyEvent
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.empresa.bancamovil.Account.QRTransfers.Models.QRTransfer
import com.empresa.bancamovil.R
import com.empresa.bancamovil.Utils.BaseActivity
import com.empresa.bancamovil.Utils.Properties
import java.text.SimpleDateFormat
import java.util.*

class VoucherQRTransfersActivity : BaseActivity() {

    var ivLogout: ImageView? = null
    var ivBack: ImageView? = null
    var tvOriginAcc: TextView? = null
    var tvAccName: TextView? = null
    var tvDestiAcc: TextView? = null
    var tvAmount: TextView? = null
    var tvDetails: TextView? = null
    var tvDateTime: TextView? = null
    var btnContinue: Button? = null
    lateinit var transfer: QRTransfer

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_voucher_qrtransfers)

        initialize()

        btnContinue!!.setOnClickListener {
            goHomeProducts()
        }
    }

    //region initialize
    private fun initialize() {
        tvOriginAcc = findViewById<TextView>(R.id.tvOriginAcc)
        tvAccName = findViewById<TextView>(R.id.tvAccName)
        tvDestiAcc = findViewById<TextView>(R.id.tvDestiAcc)
        tvAmount = findViewById<TextView>(R.id.tvAmount)
        tvDetails = findViewById<TextView>(R.id.tvDetails)
        tvDateTime = findViewById<TextView>(R.id.tvDateTime)
        btnContinue = findViewById<Button>(R.id.btnContinue)

        transfer = intent.extras.getSerializable("TRANSFER") as QRTransfer
        fillData()
    }
    //endregion initialize

    //region view
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        return if (keyCode == KeyEvent.KEYCODE_BACK) {
            return false
        } else super.onKeyDown(keyCode, event)
    }

    fun fillData(){
        if(transfer != null){
            val accSession = getValueSession(Properties.ACCOUNT_NUMBER, applicationContext, Properties.SESION_PREFERENCES)
            tvOriginAcc!!.text = transfer.accNumber
            tvAccName!!.text = getValueSession(Properties.ACCOUNT_NAME, applicationContext, Properties.SESION_PREFERENCES)
            tvDestiAcc!!.text = accSession
            tvAmount!!.text = "$" + formatNumber(transfer!!.amount)
            tvDetails!!.text = transfer!!.details

            val sdf = SimpleDateFormat("dd/M/yyyy hh:mm:ss")
            val currentDate = sdf.format(Date())

            tvDateTime!!.text = currentDate
        }
    }
    //endregion view
}
