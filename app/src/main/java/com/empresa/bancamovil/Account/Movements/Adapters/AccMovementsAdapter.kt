package com.empresa.bancamovil.Account.Movements.Adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.empresa.bancamovil.R
import android.widget.LinearLayout
import android.util.Log
import com.empresa.bancamovil.Account.Movements.Interfaces.IAccMovementsView
import com.empresa.bancamovil.Account.Movements.Models.AccMovement

class AccMovementsAdapter(
    private val movementsList: List<AccMovement>,
    private val iAccMovementsView: IAccMovementsView
) : RecyclerView.Adapter<AccMovementsAdapter.MyViewHolder>() {

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tvDescription: TextView
        var tvDate: TextView
        var tvReference: TextView
        var tvAmount : TextView
        var llMovement: LinearLayout

        init {
            tvDescription = view.findViewById(R.id.tvDescription) as TextView
            tvDate = view.findViewById(R.id.tvDate) as TextView
            tvReference = view.findViewById(R.id.tvReference) as TextView
            tvAmount = view.findViewById(R.id.tvAmount) as TextView
            llMovement = view.findViewById(R.id.llMovement) as LinearLayout
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.movements_list, parent, false)

        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val movement = movementsList.get(position)
        holder.tvDescription.text = movement.description
        holder.tvDate.text = movement.date
        holder.tvReference.text = movement.reference
        holder.tvAmount.text = movement.amount

        holder.llMovement.setOnClickListener(View.OnClickListener {
            Log.d("Llego", "al click")
            //iAccMovementsView.OnSelectAccount(account)
        })
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return movementsList.size
    }
}