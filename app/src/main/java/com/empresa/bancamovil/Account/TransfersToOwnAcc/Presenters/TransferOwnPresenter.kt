package com.empresa.bancamovil.Account.TransfersToOwnAcc.Presenters

import android.content.Context
import com.empresa.bancamovil.Account.TransfersToOwnAcc.Interfaces.ITransferOwnPresenter
import com.empresa.bancamovil.Account.TransfersToOwnAcc.Interfaces.ITransferOwnView
import com.empresa.bancamovil.Account.TransfersToOwnAcc.Models.TransferOwn
import com.empresa.bancamovil.Account.TransfersToOwnAcc.Repository.TransferOwnRepository
import com.empresa.bancamovil.Products.Models.Account

public class TransferOwnPresenter(iTransferOwnView: ITransferOwnView, context: Context) : ITransferOwnPresenter {

    private var iTransferOwnView = iTransferOwnView
    private var context = context
    private var repository = TransferOwnRepository()

    override fun getAccounts(accSession: String) {
        repository.getAccounts(accSession, this)
    }

    override fun OnGetAccountsSuccess(accountsList: List<Account>) {
        iTransferOwnView!!.OnGetAccountSuccess(accountsList)
    }

    override fun validateData(transfer: TransferOwn) {
        if(transfer.accName == ""){
            iTransferOwnView!!.OnProcesError("Por favor selecciona una cuenta")
        }else if(transfer.amount == ""){
            iTransferOwnView!!.OnProcesError("Ingresa el monto de la transferencia")
        }else if(transfer.details == ""){
            iTransferOwnView!!.OnProcesError("Ingresa el detalle de la transferencia")
        }else{
            iTransferOwnView.OnValidateData(transfer)
        }
    }

    override fun validateTransfer(transfer: TransferOwn) {
        iTransferOwnView.OnValidateData(transfer)
    }

    override fun OnProcessError(error: String) {
        iTransferOwnView!!.OnProcesError(error)
    }
}