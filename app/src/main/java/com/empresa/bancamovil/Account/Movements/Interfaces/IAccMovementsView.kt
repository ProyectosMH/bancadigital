package com.empresa.bancamovil.Account.Movements.Interfaces

import com.empresa.bancamovil.Account.Movements.Models.AccMovement

interface IAccMovementsView {
    fun OnGetAccMovementsSuccess(movementsList: List<AccMovement>)
    fun OnProcesError(error: String)
}