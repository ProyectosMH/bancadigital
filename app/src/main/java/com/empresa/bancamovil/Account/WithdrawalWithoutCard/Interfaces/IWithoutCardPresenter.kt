package com.empresa.bancamovil.Account.WithdrawalWithoutCard.Interfaces

import com.empresa.bancamovil.Account.WithdrawalWithoutCard.Models.TransferWithoutCard

interface IWithoutCardPresenter {
    fun validateData(transfer: TransferWithoutCard)
    fun validateTransfer(transfer: TransferWithoutCard)

    fun OnProcessError(error: String)
}