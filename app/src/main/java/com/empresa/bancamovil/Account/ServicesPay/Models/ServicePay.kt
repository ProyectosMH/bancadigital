package com.empresa.bancamovil.Account.ServicesPay.Models

import java.io.Serializable

class ServicePay() : Serializable {

    var accName: String = ""
    var accNumber: String = ""
    var code: String = ""
    var type: String = ""

    constructor(accName: String, accNumber: String, code: String, type: String) : this() {
        this.accName = accName
        this.accNumber = accNumber
        this.code = code
        this.type = type
    }
}