package com.empresa.bancamovil.Account.TransfersToThird.Activities

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.Window
import android.widget.*
import com.empresa.bancamovil.Account.TransfersToThird.Adapters.FavoritesListViewAdapter
import com.empresa.bancamovil.Account.TransfersToThird.Interfaces.ITransferThirdView
import com.empresa.bancamovil.Account.TransfersToThird.Models.Favorite
import com.empresa.bancamovil.Account.TransfersToThird.Presenters.TransferThirdPresenter
import com.empresa.bancamovil.R
import com.empresa.bancamovil.Utils.BaseActivity
import es.dmoral.toasty.Toasty
import android.widget.TextView
import android.widget.AdapterView
import com.empresa.bancamovil.Account.TransfersToThird.Models.TransferThird
import com.empresa.bancamovil.Utils.Properties
import com.google.android.material.textfield.TextInputEditText

class TransfersToThirdActivity : BaseActivity(), ITransferThirdView {

    private var presenter: TransferThirdPresenter? = null
    var ivLogout: ImageView? = null
    var ivBack: ImageView? = null
    var tvAcc: TextView? = null
    var tvAccAvailable: TextView? = null
    var rlFavorite: RelativeLayout? = null
    var llFavorite: LinearLayout? = null
    var tvFavName: TextView? = null
    var tvFavAcc: TextView? = null
    var tivAmount: TextInputEditText? = null
    var tivDetail: TextInputEditText? = null
    var btnContinue: Button? = null
    var accSession: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_transfers_to_third)

        initialize()
        presenter = TransferThirdPresenter(this, applicationContext)

        ivLogout!!.setOnClickListener {
            logout()
        }

        ivBack!!.setOnClickListener {
            finish()
        }

        rlFavorite!!.setOnClickListener {
            presenter!!.getFavorites()
        }

        btnContinue!!.setOnClickListener {
            var transfetThird = TransferThird()
            transfetThird.favName = tvFavName!!.text.toString()
            transfetThird.favAcc = tvFavAcc!!.text.toString()
            transfetThird.amount = tivAmount!!.text.toString()
            transfetThird.details = tivDetail!!.text.toString()
            presenter!!.validateData(transfetThird)
        }
    }

    //region initialize
    private fun initialize() {
        ivLogout = findViewById<ImageView>(R.id.ivLogout)
        ivBack = findViewById<ImageView>(R.id.ivBack)
        tvAcc = findViewById<TextView>(R.id.tvAcc)
        tvAccAvailable = findViewById<TextView>(R.id.tvAccAvailable)
        rlFavorite = findViewById<RelativeLayout>(R.id.rlFavorite)
        llFavorite = findViewById<LinearLayout>(R.id.llFavorite)
        tvFavName = findViewById<TextView>(R.id.tvFavName)
        tvFavAcc = findViewById<TextView>(R.id.tvFavAcc)
        tivAmount = findViewById<TextInputEditText>(R.id.tivAmount)
        tivDetail = findViewById<TextInputEditText>(R.id.tivDetail)
        btnContinue = findViewById<Button>(R.id.btnContinue)

        accSession = getValueSession(Properties.ACCOUNT_NUMBER, applicationContext, Properties.SESION_PREFERENCES)
        tvAcc!!.text = accSession
        tvAccAvailable!!.text = "$" + formatNumber(getValueSession(Properties.ACCOUNT_AVAILABLE, applicationContext, Properties.SESION_PREFERENCES))
    }
    //endregion initialize

    //region view
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        return if (keyCode == KeyEvent.KEYCODE_BACK) {
            return false
        } else super.onKeyDown(keyCode, event)
    }

    override fun OnGetFavoritesSuccess(favoritesList: List<Favorite>) {
        val dialog = Dialog(this, R.style.DialogListTheme)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.list_dialog)

        var tvTitle: TextView = dialog.findViewById<TextView>(R.id.tvTitle)
        tvTitle.text = "Selecciona un favorito"

        var btnCancel: Button = dialog.findViewById<Button>(R.id.btnCancel)

        var lvItems: ListView = dialog.findViewById<ListView>(R.id.lvItems)

        val favoritesAdapter = FavoritesListViewAdapter(favoritesList, applicationContext)
        lvItems.adapter = favoritesAdapter

        lvItems.setOnItemClickListener(object : AdapterView.OnItemClickListener {
            override fun onItemClick(adapter: AdapterView<*>, view: View, position: Int, arg: Long) {
                val name = view.findViewById(R.id.tvName) as TextView
                val acc = view.findViewById(R.id.tvAccount) as TextView
                tvFavName!!.text = name.text.toString()
                tvFavAcc!!.text = acc.text.toString()
                llFavorite!!.visibility = View.VISIBLE
                dialog.dismiss()
            }
        })

        btnCancel.setOnClickListener {
            tvFavName!!.text = ""
            tvFavAcc!!.text = ""
            llFavorite!!.visibility = View.GONE
            dialog.dismiss()
        }

        dialog.show()
    }

    override fun OnValidateData(transfer: TransferThird) {
        val intent = Intent(this, ValidateThirdActivity::class.java)
        intent.putExtra("TRANSFER", transfer)
        startActivity(intent)
    }

    override fun OnProcesError(error: String) {
        Toasty.error(applicationContext, error, Toast.LENGTH_SHORT, true).show()
    }
    //endregion view
}
