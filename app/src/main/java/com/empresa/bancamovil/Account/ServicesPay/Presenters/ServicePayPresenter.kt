package com.empresa.bancamovil.Account.ServicesPay.Presenters

import android.content.Context
import com.empresa.bancamovil.Account.ServicesPay.Interfaces.IServiPayPresenter
import com.empresa.bancamovil.Account.ServicesPay.Interfaces.IServiPayView
import com.empresa.bancamovil.Account.ServicesPay.Models.ServicePay
import com.empresa.bancamovil.Account.ServicesPay.Repository.ServicePayRepository

public class ServicePayPresenter(iServiPayView: IServiPayView, context: Context) :
    IServiPayPresenter {

    private var iServiPayView = iServiPayView
    private var context = context
    private var repository = ServicePayRepository()

    override fun validateData(pay: ServicePay) {
        if(pay.accName == ""){
            iServiPayView!!.OnProcesError("Por favor selecciona una cuenta")
        }else if(pay.code == ""){
            iServiPayView!!.OnProcesError("Ingresa el codigo NPE para realizar el pago")
        }else{
            iServiPayView.OnValidateData(pay)
        }
    }

    override fun validatePay(pay: ServicePay) {
        iServiPayView.OnValidateData(pay)
    }

    override fun OnProcessError(error: String) {
        iServiPayView!!.OnProcesError(error)
    }
}