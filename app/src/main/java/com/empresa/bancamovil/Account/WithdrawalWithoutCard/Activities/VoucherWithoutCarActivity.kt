package com.empresa.bancamovil.Account.WithdrawalWithoutCard.Activities

import android.os.Bundle
import android.view.KeyEvent
import android.widget.Button
import android.widget.TextView
import com.empresa.bancamovil.Account.WithdrawalWithoutCard.Models.TransferWithoutCard
import com.empresa.bancamovil.R
import com.empresa.bancamovil.Utils.BaseActivity
import java.text.SimpleDateFormat
import java.util.*

class VoucherWithoutCarActivity : BaseActivity() {

    var btnContinue: Button? = null
    var tvAccName: TextView? = null
    var tvAccNumber: TextView? = null
    var tvAmount: TextView? = null
    var tvDateTime: TextView? = null
    var transfer = TransferWithoutCard()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_voucher_without_car)

        initialize()

        btnContinue!!.setOnClickListener {
            goHomeProducts()
        }
    }

    //region initialize
    private fun initialize() {
        tvAccName = findViewById<TextView>(R.id.tvAccName)
        tvAccNumber = findViewById<TextView>(R.id.tvAccNumber)
        tvAmount = findViewById<TextView>(R.id.tvAmount)
        tvDateTime = findViewById<TextView>(R.id.tvDateTime)
        btnContinue = findViewById<Button>(R.id.btnContinue)

        transfer = intent.extras.getSerializable("TRANSFER") as TransferWithoutCard
        fillData()
    }
    //endregion initialize

    //region view
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        return if (keyCode == KeyEvent.KEYCODE_BACK) {
            return false
        } else super.onKeyDown(keyCode, event)
    }

    fun fillData(){
        if(transfer != null){
            tvAccName!!.text = transfer!!.accName
            tvAccNumber!!.text = transfer!!.accNumber
            tvAmount!!.text = "$" + formatNumber(transfer!!.amount)

            val sdf = SimpleDateFormat("dd/M/yyyy hh:mm:ss")
            val currentDate = sdf.format(Date())

            tvDateTime!!.text = currentDate
        }
    }
    //endregion view
}
