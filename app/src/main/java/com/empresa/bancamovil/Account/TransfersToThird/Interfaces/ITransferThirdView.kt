package com.empresa.bancamovil.Account.TransfersToThird.Interfaces

import com.empresa.bancamovil.Account.TransfersToThird.Models.Favorite
import com.empresa.bancamovil.Account.TransfersToThird.Models.TransferThird

interface ITransferThirdView {
    fun OnGetFavoritesSuccess(favoritesList: List<Favorite>)
    fun OnValidateData(transfer: TransferThird)
    fun OnProcesError(error: String)
}