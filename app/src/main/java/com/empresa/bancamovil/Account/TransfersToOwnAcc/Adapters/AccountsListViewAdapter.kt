package com.empresa.bancamovil.Account.TransfersToOwnAcc.Adapters

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.view.LayoutInflater
import android.widget.TextView
import com.empresa.bancamovil.Products.Models.Account
import com.empresa.bancamovil.R

class AccountsListViewAdapter(accountsList: List<Account>, context: Context) : BaseAdapter() {

    private val context = context
    private val accountsList = accountsList

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val layoutInflater :LayoutInflater = LayoutInflater.from(context)

        val view : View = layoutInflater.inflate(R.layout.item_list_view , null )
        var tvName : TextView = view.findViewById(R.id.tvName)
        var tvAccount : TextView = view.findViewById(R.id.tvAccount)

        var account : Account = accountsList[position]

        tvName.text = account.name
        tvAccount.text = account.accNumber

        return view
    }

    override fun getItem(position: Int): Any {
        return accountsList[position]
    }

    override fun getItemId(position: Int): Long {
        return  0
    }

    override fun getCount(): Int {
        return accountsList.size
    }
}