package com.empresa.bancamovil.Account.QRTransfers.Interfaces

import com.empresa.bancamovil.Account.QRTransfers.Models.QRTransfer

interface IQRTransfersView {
    fun OnValidateData(transfer: QRTransfer)
    fun OnProcesError(error: String)
}