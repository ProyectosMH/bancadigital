package com.empresa.bancamovil.Account.Movements.Presenters

import android.content.Context
import com.empresa.bancamovil.Account.Movements.Interfaces.IAccMovementsPresenter
import com.empresa.bancamovil.Account.Movements.Interfaces.IAccMovementsView
import com.empresa.bancamovil.Account.Movements.Models.AccMovement
import com.empresa.bancamovil.Account.Movements.Repository.AccMovementsRepository
import com.empresa.bancamovil.Account.ServicesPay.Models.ServicePay

public class AccMovementsPresenter(iAccMovementsView: IAccMovementsView, context: Context) :
    IAccMovementsPresenter {

    private var iAccMovementsView = iAccMovementsView
    private var context = context
    private var repository = AccMovementsRepository()

    override fun getAccMovements() {
        repository.getAccMovements(this)
    }

    override fun OnGetAccMovementsSuccess(movementsList: List<AccMovement>) {
        iAccMovementsView.OnGetAccMovementsSuccess(movementsList)
    }

    override fun OnProcessError(error: String) {
        iAccMovementsView!!.OnProcesError(error)
    }
}