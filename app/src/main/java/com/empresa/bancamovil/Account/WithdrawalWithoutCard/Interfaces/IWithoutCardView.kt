package com.empresa.bancamovil.Account.WithdrawalWithoutCard.Interfaces

import com.empresa.bancamovil.Account.WithdrawalWithoutCard.Models.TransferWithoutCard

interface IWithoutCardView {
    fun OnValidateData(transfer: TransferWithoutCard)
    fun OnProcesError(error: String)
}