package com.empresa.bancamovil.Account.Movements.Repository

import com.empresa.bancamovil.Account.Movements.Interfaces.IAccMovementsPresenter
import com.empresa.bancamovil.Account.Movements.Models.AccMovement

class AccMovementsRepository {

    fun getAccMovements(presenter: IAccMovementsPresenter) {
        try {
            val movementsList = ArrayList<AccMovement>()

            //adding some dummy data to the list
            movementsList.add(AccMovement("1002", "WENDY'S PLAZA MERLIOT", "$7.50", "+", "15/10/2020"))
            movementsList.add(AccMovement("1047", "PIZZA HUT GALERIAS", "$8.25", "+", "14/10/2020"))
            movementsList.add(AccMovement("1203", "SEGURO DE VIDA", "$1.90", "+", "12/10/2020"))
            movementsList.add(AccMovement("8563", "PAGO EFECTIVO", "$35.99", "-", "10/10/2020"))
            movementsList.add(AccMovement("5874", "NETFLIX", "$10.99", "+", "09/10/2020"))
            movementsList.add(AccMovement("8745", "GASOLINERIA UNO", "$25.00", "+", "09/10/2020"))

            presenter.OnGetAccMovementsSuccess(movementsList)
        } catch (e: Exception) {
            presenter.OnProcessError("Estamos en mantenimiento, intenta nuevamente")
        }
    }
}