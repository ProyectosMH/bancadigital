package com.empresa.bancamovil.Account.ServicesPay.Interfaces

import com.empresa.bancamovil.Account.ServicesPay.Models.ServicePay

interface IServiPayPresenter {
    fun validateData(pay: ServicePay)
    fun validatePay(pay: ServicePay)

    fun OnProcessError(error: String)
}