package com.empresa.bancamovil.Account.WithdrawalWithoutCard.Models

import java.io.Serializable

class TransferWithoutCard() : Serializable {

    var accName: String = ""
    var accNumber: String = ""
    var amount: String = ""

    constructor(accName: String, accNumber: String, amount: String, details: String) : this() {
        this.accName = accName
        this.accNumber = accNumber
        this.amount = amount
    }
}