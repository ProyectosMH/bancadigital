package com.empresa.bancamovil.Login.Interfaces

interface ILoginView {
    fun OnAuthSuccess()
    fun OnEnrollBiometricAuthSuccess()
    fun OnAuthBiometricSuccess()
    fun showBiometricDialog()
    fun OnProcesError(error: String)
}