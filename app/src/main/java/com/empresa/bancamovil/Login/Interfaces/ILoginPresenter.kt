package com.empresa.bancamovil.Login.Interfaces

interface ILoginPresenter {
    fun validateCredentials(user: String, pass: String, proces: String)
    fun loginWithBiometric(user: String, pass: String)
    fun validateEnrolledBiometric(user: String, pass: String)

    fun OnAuthSuccess()
    fun OnProcessError(error: String)
    fun OnAuthBiometricSuccess()
}