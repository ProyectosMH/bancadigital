package com.empresa.bancamovil.Login.Repository

import com.empresa.bancamovil.Login.Presenters.LoginPresenter

class LoginRepository {
    fun autentificacion(user: String, pass: String, isBiometricAuth: String, presenter: LoginPresenter) {
        try {
            if(user.equals("MARVIN10") && pass.equals("123")){
                if(isBiometricAuth == "Yes"){
                    presenter.OnAuthBiometricSuccess()
                }else{
                    presenter.OnAuthSuccess()
                }
            }else if(user.equals("HECTOR") && pass.equals("admin")){
                if(isBiometricAuth == "Yes"){
                    presenter.OnAuthBiometricSuccess()
                }else{
                    presenter.OnAuthSuccess()
                }
            }else if(user.equals("LUIS") && pass.equals("admin")){
                if(isBiometricAuth == "Yes"){
                    presenter.OnAuthBiometricSuccess()
                }else{
                    presenter.OnAuthSuccess()
                }
            }else{
                presenter.OnProcessError("Usuario o contraseña incorrecta")
            }
        } catch (e: Exception) {
            presenter.OnProcessError("Estamos en mantenimiento, intenta nuevamente")
        }

    }
}