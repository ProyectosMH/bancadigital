package com.empresa.bancamovil.Login.Presenters

import android.content.Context
import com.empresa.bancamovil.Login.Interfaces.ILoginPresenter
import com.empresa.bancamovil.Login.Interfaces.ILoginView
import com.empresa.bancamovil.Login.Repository.LoginRepository

public class LoginPresenter(iLoginView: ILoginView, context: Context) : ILoginPresenter {

    private var iLoginView = iLoginView
    private var context = context
    private var proces: String = ""
    private var repository = LoginRepository()

    override fun validateCredentials(user: String, pass: String, proces: String) {
        if(user.equals("") && pass.equals("")){
            iLoginView!!.OnProcesError("Ingresa tu usuario y contraseña")
        }else if(user.equals("")){
            iLoginView!!.OnProcesError("Ingresa tu usuario")
        }else if(pass.equals("")){
            iLoginView!!.OnProcesError("Ingresa tu contraseña")
        }else{
            this.proces = proces
            repository.autentificacion(user, pass, "No", this)
        }
    }

    override fun validateEnrolledBiometric(user: String, pass: String) {
        if(user.equals("") && pass.equals("")){
            iLoginView!!.OnProcesError("Ingresa tu usuario y contraseña")
        }else if(user.equals("")){
            iLoginView!!.OnProcesError("Ingresa tu usuario")
        }else if(pass.equals("")){
            iLoginView!!.OnProcesError("Ingresa tu contraseña")
        }else{
            iLoginView!!.showBiometricDialog()
        }
    }

    override fun loginWithBiometric(user: String, pass: String) {
        if(user.equals("") && pass.equals("")){
            iLoginView!!.OnProcesError("Usuario y contraseña no encontradas")
        }else if(user.equals("")){
            iLoginView!!.OnProcesError("Usuario no encontrado")
        }else if(pass.equals("")){
            iLoginView!!.OnProcesError("Contraseña no encontrada")
        }else{
            repository.autentificacion(user, pass, "Yes", this)
        }
    }

    override fun OnAuthSuccess() {
        if(this.proces.equals("Biometric")){
            iLoginView!!.OnEnrollBiometricAuthSuccess()
        }else{
            iLoginView!!.OnAuthSuccess()
        }
    }

    override fun OnAuthBiometricSuccess() {
        iLoginView!!.OnAuthBiometricSuccess()
    }

    override fun OnProcessError(error: String) {
        iLoginView!!.OnProcesError(error)
    }
}