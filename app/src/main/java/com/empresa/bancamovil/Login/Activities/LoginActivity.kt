package com.empresa.bancamovil.Login.Activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.Toast
import com.empresa.bancamovil.Login.Interfaces.IBiometricAuthView
import com.empresa.bancamovil.Login.Interfaces.ILoginView
import com.empresa.bancamovil.Products.Activities.ProductsActivity
import com.empresa.bancamovil.R
import de.hdodenhof.circleimageview.CircleImageView
import es.dmoral.toasty.Toasty
import com.empresa.bancamovil.Login.Presenters.LoginPresenter
import com.empresa.bancamovil.MainActivity
import com.empresa.bancamovil.Utils.BaseActivity
import com.empresa.bancamovil.Utils.Properties
import com.google.android.material.textfield.TextInputEditText

class LoginActivity : BaseActivity(), ILoginView, IBiometricAuthView  {

    private var presenter: LoginPresenter? = null
    var civBack: CircleImageView? = null
    var tivUser: TextInputEditText? = null
    var tivePass: TextInputEditText? = null
    var btnLogin: Button? = null
    var llBiometrica: LinearLayout? = null
    var user: String = ""
    var pass: String = ""
    var process: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        initialize()
        presenter = LoginPresenter(this, applicationContext)

        civBack!!.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()
        }

        btnLogin!!.setOnClickListener {
            ProgresbarStar()
            this.process = "Normal"
            presenter!!.validateCredentials(tivUser!!.text.toString(), tivePass!!.text.toString(), this.process)
        }

        llBiometrica!!.setOnClickListener {
            this.process = "Biometric"
            if(hasEnrolledFinger){
                showBiometricPrompt()
            }else{
                ProgresbarStar()
                presenter!!.validateEnrolledBiometric(tivUser!!.text.toString(), tivePass!!.text.toString())
            }
        }

        if(verifyFingerDevice()){
            biometricEnabled()
        }else{
            llBiometrica!!.visibility = View.GONE
        }
    }

    //region initialize
    private fun initialize() {
        civBack = findViewById<CircleImageView>(R.id.civBack)
        tivUser = findViewById<TextInputEditText>(R.id.tivUser)
        tivePass = findViewById<TextInputEditText>(R.id.tivePass)
        btnLogin = findViewById<Button>(R.id.btnLogin)
        llBiometrica = findViewById<LinearLayout>(R.id.llBiometrica)

        if(verifyFingerDevice()){
            llBiometrica?.visibility = View.VISIBLE
        }else{
            llBiometrica?.visibility = View.GONE
        }
    }
    //endregion initialize

    //region view
    override fun OnBiometricAuthSuccess() {
        ProgresbarStar()
        if(hasEnrolledFinger){
            this.user = getValueSession(Properties.USER_BIOMETRIC, applicationContext, Properties.BIOMETRIC_PREFERENCES)
            this.pass = getValueSession(Properties.PASS_BIOMETRIC, applicationContext, Properties.BIOMETRIC_PREFERENCES)
            presenter!!.loginWithBiometric(this.user, this.pass)
        }else{
            presenter!!.validateCredentials(tivUser!!.text.toString(), tivePass!!.text.toString(), this.process)
        }
    }

    override fun OnAuthSuccess() {
        setUserLoginSession(tivUser!!.text.toString(), tivePass!!.text.toString(), "credential")
        ProgresbarFinish()
        val intent = Intent(this, ProductsActivity::class.java)
        startActivity(intent)
        finish()
    }

    override fun OnAuthBiometricSuccess() {
        setUserLoginSession(this.user, this.pass, "")
        ProgresbarFinish()
        val intent = Intent(this, ProductsActivity::class.java)
        startActivity(intent)
        finish()
    }

    override fun OnEnrollBiometricAuthSuccess() {
        setUserLoginSession(tivUser!!.text.toString(), tivePass!!.text.toString(), "biometric")
        ProgresbarFinish()
        val intent = Intent(this, ProductsActivity::class.java)
        startActivity(intent)
        finish()
    }

    override fun showBiometricDialog() {
        this.process = "Biometric"
        ProgresbarFinish()
        showBiometricPrompt()
    }

    override fun OnProcesError(error: String) {
        ProgresbarFinish()
        Toasty.error(applicationContext, error, Toast.LENGTH_SHORT, true).show()
    }
    //endregion view
}
