package com.empresa.bancamovil.Geolocation.Interfaces

import com.empresa.bancamovil.Geolocation.Models.Location

interface IGeolocationView {
    fun OnGetLocationsSuccess(locationsList: List<Location>)
    fun OnProcesError(error: String)
}