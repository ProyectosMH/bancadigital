package com.empresa.bancamovil.Geolocation.Models

import java.io.Serializable

class Location() : Serializable {

    var name: String = ""
    var address: String = ""
    var latitude: String = ""
    var longitude: String = ""

    constructor(name: String, address: String, latitude: String, longitude: String) : this() {
        this.name = name
        this.address = address
        this.latitude = latitude
        this.longitude= longitude
    }
}