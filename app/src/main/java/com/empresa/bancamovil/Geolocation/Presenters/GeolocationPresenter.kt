package com.empresa.bancamovil.Geolocation.Presenters

import android.content.Context
import com.empresa.bancamovil.Geolocation.Interfaces.IGeolocationPresenter
import com.empresa.bancamovil.Geolocation.Interfaces.IGeolocationView
import com.empresa.bancamovil.Geolocation.Models.Location
import com.empresa.bancamovil.Geolocation.Repository.GeolocationRepository

public class GeolocationPresenter(iGeolocationView: IGeolocationView, context: Context) : IGeolocationPresenter {

    private var iGeolocationView = iGeolocationView
    private var context = context
    private var repository = GeolocationRepository()

    override fun getLocations() {
        repository.getLocations(this)
    }

    override fun OnGetLocationsSuccess(locationsList: List<Location>) {
        iGeolocationView!!.OnGetLocationsSuccess(locationsList)
    }

    override fun OnProcessError(error: String) {
        iGeolocationView!!.OnProcesError(error)
    }
}