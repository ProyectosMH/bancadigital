package com.empresa.bancamovil.Geolocation.Activities

import android.Manifest
import android.os.Bundle
import android.widget.Toast
import com.empresa.bancamovil.Geolocation.Interfaces.IGeolocationView
import com.empresa.bancamovil.Geolocation.Models.Location
import com.empresa.bancamovil.Geolocation.Presenters.GeolocationPresenter
import com.empresa.bancamovil.R
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import es.dmoral.toasty.Toasty
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.LatLng
import android.location.LocationManager
import android.content.Context
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import com.google.android.gms.maps.CameraUpdateFactory
import android.graphics.Bitmap
import android.graphics.Canvas
import androidx.core.content.ContextCompat
import androidx.annotation.DrawableRes
import androidx.fragment.app.FragmentActivity
import com.google.android.gms.maps.model.BitmapDescriptor

class GeolocationActivity : FragmentActivity(), OnMapReadyCallback, IGeolocationView {

    private var presenter: GeolocationPresenter? = null
    lateinit var mMap: GoogleMap
    lateinit var mMapFragment: SupportMapFragment
    var locationsList: List<Location> = ArrayList<Location>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_geolocation)

        val permissions = arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION)
        ActivityCompat.requestPermissions(this, permissions,101)
        val permissions2 = arrayOf(android.Manifest.permission.ACCESS_COARSE_LOCATION)
        ActivityCompat.requestPermissions(this, permissions2,102)

        initialize()
        presenter = GeolocationPresenter(this, applicationContext)

        presenter!!.getLocations()
    }

    //region initialize
    private fun initialize() {
        mMapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mMapFragment!!.getMapAsync(this)
    }
    //endregion initialize

    //region view
    override fun OnGetLocationsSuccess(locationsList: List<Location>) {
        this.locationsList = locationsList
        onMapReady(mMap)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        this.mMap = googleMap

        if(locationsList != null){
            var name: String
            var address: String
            var latitude: String
            var longitude: String
            val markerOptions = MarkerOptions()

            for (location in this.locationsList) {
                name = location.name
                address = location.address
                latitude = location.latitude
                longitude = location.longitude

                markerOptions.icon(bitmapDescriptorFromVector(this, R.drawable.ic_place))

                var latituded: Double = latitude.toDouble()
                var longituded: Double = longitude.toDouble()

                val blueMarket = LatLng(latituded, longituded)
                markerOptions.title(name)
                markerOptions.position(blueMarket)
                markerOptions.snippet(address)
                val m = mMap!!.addMarker(markerOptions)
            }
        }

        val service = getSystemService(Context.LOCATION_SERVICE) as LocationManager

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return
        }

        mMap!!.isMyLocationEnabled = true
        mMap!!.uiSettings.isMyLocationButtonEnabled = true
        mMap!!.uiSettings.isCompassEnabled = true

        val location = service.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER)
        val userLocation: LatLng
        if (location == null) {
            userLocation = LatLng(13.6914782, -89.2146939)
        } else {
            userLocation = LatLng(location.latitude, location.longitude)
        }

        mMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(userLocation, 12f))
    }

    private fun bitmapDescriptorFromVector(context: Context, @DrawableRes vectorResId: Int): BitmapDescriptor {
        val vectorDrawable = ContextCompat.getDrawable(context, vectorResId)
        vectorDrawable!!.setBounds(
            0,
            0,
            vectorDrawable.intrinsicWidth,
            vectorDrawable.intrinsicHeight
        )
        val bitmap = Bitmap.createBitmap(
            vectorDrawable.intrinsicWidth,
            vectorDrawable.intrinsicHeight,
            Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(bitmap)
        vectorDrawable.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }

    override fun OnProcesError(error: String) {
        Toasty.error(applicationContext, error, Toast.LENGTH_SHORT, true).show()
    }
    //endregion view
}
