package com.empresa.bancamovil.Geolocation.Repository

import com.empresa.bancamovil.Geolocation.Interfaces.IGeolocationPresenter
import com.empresa.bancamovil.Geolocation.Models.Location
import android.os.CountDownTimer
import java.util.*
import kotlin.collections.ArrayList


class GeolocationRepository {

    fun getLocations(presenter: IGeolocationPresenter) {
        try {
            val locationsList = ArrayList<Location>()

            //adding some dummy data to the list
            locationsList.add(Location("Agencia San Salvador", "Tercera calle poniente y septima av norte local #2 edif. #308 San Salvador", "13.701294", "-89.194625"))
            locationsList.add(Location("Agencia Santa tecla", "Avenida Dr. Manuel Gallardo, Santa Tecla", "13.6752357", "-89.2875419"))
            locationsList.add(Location("Agencia Santa Elena", "Bulevar Orden De Malta, Santa Elena", "13.663646", "-89.260635"))
            locationsList.add(Location("Agencia Escalon", "89 Y 91 paseo general escalón #4646 San Salvador CP, 1101", "13.7030775", "-89.2448618"))
            locationsList.add(Location("Agencia Soyapango", "Bulevar del Ejercito Nacional, Soyapango, San Salvador", "13.6999004", "-89.1529339"))

            val countDownTimer = object : CountDownTimer(3000, 1000) {
                override fun onTick(millisUntilFinished: Long) {

                }

                override fun onFinish() {
                    presenter.OnGetLocationsSuccess(locationsList)
                }
            }.start()


        } catch (e: Exception) {
            var error = e.message
            presenter.OnProcessError("Estamos en mantenimiento, intenta nuevamente")
        }
    }
}