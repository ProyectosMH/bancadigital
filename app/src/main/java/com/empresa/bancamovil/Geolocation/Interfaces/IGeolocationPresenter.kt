package com.empresa.bancamovil.Geolocation.Interfaces

import com.empresa.bancamovil.Geolocation.Models.Location

interface IGeolocationPresenter {
    fun getLocations()

    fun OnGetLocationsSuccess(locationsList: List<Location>)
    fun OnProcessError(error: String)
}