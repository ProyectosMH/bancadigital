package com.empresa.bancamovil

import android.content.Intent
import android.os.Bundle
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.core.app.ActivityCompat
import com.empresa.bancamovil.Geolocation.Activities.GeolocationActivity
import com.empresa.bancamovil.Login.Activities.LoginActivity


class MainActivity : AppCompatActivity() {

    var cvLogin: CardView? = null
    var llLogin: LinearLayout? = null
    var llGeolocation: LinearLayout? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val permissions = arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION)
        ActivityCompat.requestPermissions(this, permissions,101)
        val permissions2 = arrayOf(android.Manifest.permission.ACCESS_COARSE_LOCATION)
        ActivityCompat.requestPermissions(this, permissions2,102)

        initialize()

        llLogin!!.setOnClickListener {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }

        llGeolocation!!.setOnClickListener {
            val intent = Intent(this, GeolocationActivity::class.java)
            startActivity(intent)
        }
    }

    //region initialize
    private fun initialize() {
        cvLogin = findViewById<CardView>(R.id.cvLogin)
        llLogin = findViewById<LinearLayout>(R.id.llLogin)
        llGeolocation = findViewById<LinearLayout>(R.id.llGeolocation)
    }
    //endregion initialize
}
